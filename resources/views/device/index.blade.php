@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
 
<section class="content">
  <div class="container-fluid">
    @if(count ($errors) >0)
            <div class="alert alert-danger">
              <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
              </ul>
            </div>
          @endif
    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Router</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('device_store')}}">
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3" style="clear: both;float: left;">Serial Number</label>
                    <input type="text" name="serial_number" class="form-control col-md-4" placeholder="Serial Number" required>
                  </div>
                </div>
                  <!-- /.form-group -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Company Name</label>
                    <input type="text" class="form-control col-md-4" name="company_name" placeholder="Company Name" required>
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>
                <!-- /.col -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Model</label>
                    <input type="text" class="form-control col-md-4" name="model" placeholder="Model" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Price</label>
                    <input type="Number" class="form-control col-md-4" name="price" placeholder="Price" required>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>

              <!--  -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Stock Status</label>
                    <input type="text" class="form-control col-md-4" name="stock_status" placeholder="Stock Status" required>
                  </div>
                </div>
                  <!-- /.form-group -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Description</label>
                    <input type="text" class="form-control col-md-4" name="description" placeholder="Description" required>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

              </div>
                 <!-- /.col -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Ports</label>
                    <input type="text" class="form-control col-md-4" name="ports" placeholder="Ports" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Created By</label>
                    <input type="text" class="form-control col-md-4" name="created_by" placeholder="Created By" required>
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>
                 <!-- /.col -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Allocated By</label>
                    <input type="text" class="form-control col-md-4" name="allocated_by" placeholder="Allocated By" required>
                  </div>
                </div>
              </div>

              <!-- /.row -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Routers</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="device" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Serial</th>
                  <th>Ports</th>
                  <th>Price</th>
                  <th>Description</th>
                  <th>Located By</th>
                  <th>Action</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($data["devices"] as $devices)
                <tr>
                  <td>{{$devices->serial_number}}</td>
                  <td>{{$devices->ports}}</td>
                  <td>{{$devices->price}}</td>
                  <td>{{$devices->description}}</td>
                  <td>{{$devices->allocated_by}}</td>
                  <td>
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_device({{$devices->id}})">
                        Edit
                      </button>
                  </td>
                </tr>
                
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Serial</th>
                  <th>Ports</th>
                  <th>Price</th>
                  <th>Description</th>
                  <th>Allocated By</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

    <div class="modal fade" id="EditModal-default">
      <form method="POST" action="{{route('device_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="Modal_title">Edit Device</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Serial</label>
                  <input type="text" class="form-control col-md-8" id="serial_number" name="serial_number" placeholder="Serial Number">
                </div>
           
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Company Name</label>
                  <input type="text" class="form-control col-md-8" id="company_name" name="company_name" placeholder="Company Name">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Model</label>
                  <input type="text" class="form-control col-md-8" id="model" name="model" placeholder="Model">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Price</label>
                  <input type="text" class="form-control col-md-8" id="price" name="price" placeholder="Price">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Ports</label>
                  <input type="text" class="form-control col-md-8" id="ports" name="ports" placeholder="Ports">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Description</label>
                  <input type="text" class="form-control col-md-8" id="description" name="description" placeholder="Description">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Stock Status</label>
                  <input type="text" class="form-control col-md-8" id="stock_status" name="stock_status" placeholder="Stock Status">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Created By</label>
                  <input type="text" class="form-control col-md-8" id="created_by" name="created_by" placeholder="Created By">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Allocated By</label>
                  <input type="text" class="form-control col-md-8" id="allocated_by" name="allocated_by" placeholder="Allocated By">
                </div>
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
function edit_device(id)
{
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/device/edit') }}",
                  method: 'POST',
                  data: {
                    id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var serial_number = result.serial_number;                  
                     $("#serial_number").val(serial_number);

                     var company_name = result.company_name;                     
                     $("#company_name").val(company_name);

                     var model = result.model;                     
                     $("#model").val(model);

                     var price = result.price;                     
                     $("#price").val(price);

                     var stock_status = result.stock_status;                     
                     $("#stock_status").val(stock_status);

                     var ports = result.ports;                     
                     $("#ports").val(ports);

                     var description = result.description;                     
                     $("#description").val(description);

                     var allocated_by = result.allocated_by;                     
                     $("#allocated_by").val(allocated_by);

                     var created_by = result.created_by;                     
                     $("#created_by").val(created_by);
                  }});
}

</script>
<script>
  $(function () {
    $("#device").DataTable();
  });
</script>
@endsection