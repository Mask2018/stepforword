@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
 
<section class="content">
  <div class="container-fluid"> 
    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Router</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('device_store')}}">
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3" style="clear: both;float: left;">IP</label>
                    <input type="text" name="ip" class="form-control col-md-4" placeholder="IP" required>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Ports</label>
                    <input type="text" class="form-control col-md-4" name="ports" placeholder="Ports" required>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Status</label>
                    <input type="text" class="form-control col-md-4" name="status" placeholder="Status" required>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Created By</label>
                    <input type="text" class="form-control col-md-4" name="created_by" placeholder="Created By" required>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Allocated By</label>
                    <input type="text" class="form-control col-md-4" name="allocated_by" placeholder="Allocated By" required>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Routers</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="device" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>IP</th>
                  <th>Ports</th>
                  <th>Status</th>
                  <th>Created By</th>
                  <th>Located By</th>
                  <th>Action</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($data["devices"] as $devices)
                <tr>
                  <td>{{$devices->ip}}</td>
                  <td>{{$devices->ports}}</td>
                  <td>{{$devices->status}}</td>
                  <td>{{$devices->created_by}}</td>
                  <td>{{$devices->allocated_by}}</td>
                  <td>
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_device({{$devices->id}})">
                        Edit
                      </button>
                  </td>
                </tr>
                
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>IP</th>
                  <th>Ports</th>
                  <th>Status</th>
                  <th>Created By</th>
                  <th>Allocated By</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

    <div class="modal fade" id="EditModal">
      <form method="POST" action="{{route('device_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="Modal_title">Edit Device</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">IP</label>
                  <input type="text" class="form-control col-md-8" id="ip" name="ip" placeholder="IP">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Ports</label>
                  <input type="text" class="form-control col-md-8" id="ports" name="ports" placeholder="Ports">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Status</label>
                  <input type="text" class="form-control col-md-8" id="status" name="status" placeholder="Status">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Created By</label>
                  <input type="text" class="form-control col-md-8" id="created_by" name="created_by" placeholder="Created By">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Allocated By</label>
                  <input type="text" class="form-control col-md-8" id="allocated_by" name="allocated_by" placeholder="Allocated By">
                </div>
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
function EditModal(id)
{
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/device/edit') }}",
                  method: 'POST',
                  data: {
                    id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var ip = result.ip;                     
                     $("#ip").val(ip);

                     var status = result.status;                     
                     $("#status").val(status);

                     var ports = result.ports;                     
                     $("#ports").val(ports);

                     var located_by = result.located_by;                     
                     $("#located_by").val(located_by);

                     var created_by = result.created_by;                     
                     $("#created_by").val(created_by);
                  }});
}

</script>
<script>
  $(function () {
    $("#device").DataTable();
  });
</script>
@endsection