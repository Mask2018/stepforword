@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
 <style>
   .select2-container--default .select2-selection--single{
    border:transparent;
    height: 37px;
    border-bottom: 1px solid #ced4da;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered{
    letter-spacing: 0.5;
  }
   .form-control:disabled, .form-control[readonly]{
    background-color: #F6F8FE;
   }
   input.border_null{
    border-bottom: none;
   }
   th input.borderinput_null{
    background-color: white;
    border-bottom: none;
    border:transparent;
   }
  input[type=number].paid::-webkit-inner-spin-button, 
  input[type=number].paid::-webkit-outer-spin-button{  
   opacity: 0;
  }
  #right_block{
    position: static;
  }

 </style>

<section class="content">
  <div class="container-fluid"> 
    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Generate Invoice</h3>
            </div> 
              <div class="card-body">                             
                <div class="col-md-8">
                    <div class="form-group float-left col-sm-12 col-md-4">
                        <select class="select2 form-control" id="product">
                          <option value="">Select Product</option>
                          @foreach($data["product"] as $products)
                          <option value="{{ $products->id }}" data-name="{{ $products->name }}" data-price="{{ $products->sale_price }}" > {{ $products->name }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group float-left col-sm-12 col-md-4">
                      <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Quantity" min="1" required>
                    </div>
                    <div class="form-group float-left col-sm-12 col-md-4">
                      <input type="text" class="form-control border_null" id="autoamount" name="paid_amount" placeholder="Amount" value="" readonly>
                    </div>
                </div>
                <div class="col">                  
                    <button type="submit" id="AddButton" class="btn btn-primary float-left ml-2">Submit
                    </button>  
                </div>
                
              </div>

              <!-- /.card-header -->
              <div class="card-body col-md-12">
                <div class="col-md-9 float-left">
                 <table id="order_table" class="table table-bordered table-striped float-left">
                  <thead class="card-header">
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Rate</th>   
                    <th>Quantity</th>
                    <th>Amount</th>   
                    <th>Action</th>             
                  </tr>
                  </thead>
                  <tbody id="order_table_body">
                  </tbody>

                  <tfoot>
                    <tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th style="font-weight: lighter;">Sub Total :<input type="text"class="borderinput_null" name="sub_amount" id="sub_amount" disabled></th>
                      <th></th>
                    </tr>
                  </tfoot>
                 </table>
                </div>           

                <div class="col col-md-3 col float-right pl-md-3" id="right_block">
                  
                  <select class="form-control select2 col-7 col-sm-10 mr-auto" id="customer" name="customer_id" required>
                    <option value="">Select Customer</option>
                    @foreach($data["customer"] as $customers)
                    <option value="{{ $customers->id }}" data-name="{{ $customers->name }}">{{ $customers->name }}</option>
                    @endforeach
                  </select>
                  <input type="number" class="form-control col-7 col-sm-10 mt-1 mb-1" name="discount" id="discount" placeholder="Discount" min="0" required>                    
                  <input type="number" class="form-control col-7 col-sm-10 mb-2 paid" name="paid" id="paid" min="0" placeholder="Received Amount">              
                  <input type="number" class="form-control col-7 col-sm-10 mb-1 border_null" name="balance" id="balance" placeholder="Total" readonly>                    
                  <button type="submit" id="SaveButton" class="btn-primary btn col-7 col-sm-10 mt-1">Save</button>

                </div>
              </div>
    </div>
              <!-- /.card-body -->
  </div>
</section>



@endsection
@section('content_js')
<script>
  

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
  });


  //on change product
  $('#product').on('change', function (e) {
    var id = this.value;
    var price = $(this).find(':selected').attr('data-price');
    // alert(price);
    //var assign to input field
    $("#autoamount").attr("value", price);

  });


/*dynamic table */
        $("#AddButton").click(function()//Function to Select Product And Quantity
          {
           
            var table = $("#order_table");
            var id = $("#product").find(':selected').attr('value');
            var product_name =  $("#product").find(':selected').attr('data-name');
            var price = $("#product").find(':selected').attr('data-price');
            var quantity = $("#quantity").val();
            var TotalItems = 0;
            var amount = quantity*price;           

            var Row = "";
            //document.getElementById("OrderProduct").options[selIndex].hidden = true;
            TotalItems = TotalItems+1;
            Row += "<tr id='"+TotalItems+"' >";
            Row +="<td>"+ id+ "</td>";
            Row +="<td>"+ product_name+ "</td>";
            Row +="<td>"+ price+ "</td>";
            Row +="<td>"+quantity+" </td>";
            Row +="<td>"+ amount + "</td>";
            Row +="<td class='d-print-none'> <input type='button' Value='X' class='Delete btn btn-danger ' id="+TotalItems+"> </td>";
            Row +="</tr>";

            table.append(Row);
            SubAmountCalculator();
           
        });

    $(document).on('click','.Delete',function Delete(){
      var del_id= $(this).attr('id');
      var $ele = $(this).parent().parent();
      $ele.fadeOut().remove();
    });


  function SubAmountCalculator()
    {
      var calAmount = 0;
      var sum = 0;
      var sub_amount=0;
      var TotalRows = document.getElementById("order_table_body").rows.length;
      for( i = 0; i<TotalRows; i++)
        {
          sum= document.getElementById("order_table_body").rows[i].cells.item(4).innerHTML;
          calAmount = parseFloat(sum)+parseFloat(calAmount);
          calAmount = calAmount.toFixed(2);/*after point value*/
        }
      /*document.getElementById("total_amount").innerHTML=calAmount;*/
      document.getElementById("sub_amount").value = calAmount; /*calculate quantity total balance*/
    }

  $("#discount").on("change", function() {
    var ret = parseInt($("#sub_amount").val()) - parseInt($("#discount").val() || '0')
    $("#balance").val(ret);
  });

  $('#customer').on('change', function (e) {
    var customer_id = this.value;
    var customername = $(this).find(':selected').attr('data-name');
   // var customer_id =$(this).find(':selected').attr('data-value');
    /* $('#customer').val("customer_id");*/
    // alert(customername);
  });

  $("#SaveButton").click(function Order()//Function to Select Product And Quantity
    { 
      var customer_id = $("#customer").val();
          
          var invoice_detail_data = [];
          var invoice_data = [];
          
          // $(this).attr("disabled", true);
          var inv = {

            'customer_id' : customer_id,
            'sub_amount'  : $("#sub_amount").val(),
            'discount'    : $("#discount").val(),
            'paid'        : $("#paid").val(),
            'balance'     :$("#paid").val() - $("#sub_amount").val(),
            'total'       :$("#sub_amount").val() - $("#discount").val(),
          };
              invoice_data.push(inv);

             $('#order_table tr').each(function(row,tr){

              if($(tr).find('td:eq(0)').text() == "")
              {

              }
              else
              {
                  var details = 
                  {
                   'product_id' : $(tr).find('td:eq(0)').text(),
                   'order_quantity' : $(tr).find('td:eq(3)').text(),
                   'unit_rate' : $(tr).find('td:eq(2)').text(),
                   'product_rate' : $(tr).find('td:eq(4)').text(),
                  };
                  invoice_detail_data.push(details);
              }
          });
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/invoice/store') }}",
                  method: 'POST',
                  data: {'invoice_data':invoice_data, 'invoice_detail_data':invoice_detail_data},
                  success: function(result){
                     window.location = '/invoice/view/' + result;
                    }});
          });
</script> 
@endsection