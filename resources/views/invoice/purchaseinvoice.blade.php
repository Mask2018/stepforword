@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
<style>
   .select2-container--default .select2-selection--single{
    border:transparent;
    height: 37px;
    border-bottom: 1px solid #ced4da;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered{
      line-height: initial;
   }
 </style>

<section class="content">
  <div class="container-fluid">
    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Generate Purchase Invoice</h3>
            </div>
              <div class="card-body">                             
                <div class="col-md-8">
                    <div class="form-group float-left col-sm-12 col-md-4">
                        <select class="form-control select2" id="product">
                          <option value="">Select Product</option>
                          @foreach($data["product"] as $products)
                          <option value="{{ $products->id }}" data-name="{{ $products->name }}" data-price="{{ $products->sale_price }}" > {{ $products->name }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group float-left col-sm-12 col-md-4">
                      <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Quantity" required min="1">
                    </div>
                    <div class="form-group float-left col-sm-12 col-md-4">
                      <input type="text" class="form-control" id="autoamount" name="paid_amount" placeholder="Amount" value="" readonly>
                    </div>
                </div>
                <div class="col">
                  
                      <button type="submit" id="AddButton" class="btn btn-primary float-left ml-2">Submit</button>
                                    
                </div>
              </div>

              <!-- /.card-header -->
              <div class="card-body col-md-12">
                <div class="col-md-9 float-left">
                 <table id="order_table" class="table table-bordered table-striped float-left">
                  <thead class="card-header">
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Rate</th>   
                    <th>Quantity</th>
                    <th>Amount</th>   
                    <th>Action</th>             
                  </tr>
                  </thead>
                  <tbody id="order_table_body">

                  </tbody>
                  <tfoot>
                  <!-- <tr>
                    <td></td>
                    <td><input type="text" name="totalquantitys" id="totalquantitys"></td>
                    <td><input type="text" name="totalamounts" id="totalamounts"></td>
                  </tr> -->
                  </tfoot>
                 </table>
                </div>           

                <div class="col col-md-3 float-right sticky-top pl-3">
                  <select class="form-control col select2" id="vendor" name="vendor_id">
                    <option value="">SelectVendor</option>
                    @foreach($data["vendor"] as $vendors) <!-- model name customer -->
                    <option value="{{ $vendors->id }}" data-name="{{ $vendors->name }}">{{ $vendors->name }}</option>
                    @endforeach
                  </select>
<!-- fix value set -->
                  <input type="text" name="balance"  value="0" style="display: none;">
                  <input type="text" name="paid"     value="0" style="display: none;">
                  <input type="text" name="discount" value="0" style="display: none;">
                  <input type="text" name="total"    value="0" style="display: none;">
                  
                  <label class="col mt-2">Amount:</label>
                 
                    <input type="text" name="amount" class="col ml-0 mb-2" id="TotalAmount1" disabled>
                 
                 <button type="submit" id="SaveButton" class="btn-primary btn col-md-12"> Save </button>
                </div> 
              </div>
    </div>
              <!-- /.card-body -->
  </div>
</section>



@endsection
@section('content_js')
<script>

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  });


  //on change product
  $('#product').on('change', function (e) {
    var id = this.value;
    var price = $(this).find(':selected').attr('data-price');
    // alert(price);
    //var assign to input field
    $("#autoamount").attr("value", price);
  });


/*dynamic table */
        $("#AddButton").click(function()//Function to Select Product And Quantity
          {
            var table = $("#order_table");
            var id = $("#product").find(':selected').attr('value');
            var product_name =  $("#product").find(':selected').attr('data-name');
            var price = $("#product").find(':selected').attr('data-price');
            var quantity = $("#quantity").val();
            var TotalItems = 0;
            var amount = quantity*price;           

            var Row = "";
            //document.getElementById("OrderProduct").options[selIndex].hidden = true;
            TotalItems = TotalItems+1;
            Row += "<tr id='"+TotalItems+"' >";
            Row +="<td>"+ id+ "</td>";
            Row +="<td>"+ product_name+ "</td>";
            Row +="<td>"+ price+ "</td>";
            Row +="<td>"+quantity+" </td>";
            Row +="<td>"+ amount + "</td>";
            Row +="<td class='d-print-none'> <input type='button' Value='X' class='Delete btn btn-danger ' id="+TotalItems+"> </td>";
            Row +="</tr>";

            table.append(Row);
            TotalAmountCalculator();
        });

    $(document).on('click','.Delete',function Delete(){
      var del_id= $(this).attr('id');
      var $ele = $(this).parent().parent();
      $ele.fadeOut().remove();
    });


  function TotalAmountCalculator()
    {
      var calAmount = 0;
      var sum = 0;
      var TotalRows = document.getElementById("order_table_body").rows.length;
      for( i = 0; i<TotalRows; i++)
        {
          sum= document.getElementById("order_table_body").rows[i].cells.item(4).innerHTML;
          calAmount = parseFloat(sum)+parseFloat(calAmount);
          calAmount = calAmount.toFixed(2);/*after point value*/
        }
      /*document.getElementById("TotalAmount").innerHTML=calAmount;*/
      calAmount=calAmount-1;
      document.getElementById("TotalAmount1").value = calAmount;
    }

  $('#vendor').on('change', function (e) {
    var vendor_id = this.value;
    var vendorname = $(this).find(':selected').attr('data-name');
   // var customer_id =$(this).find(':selected').attr('data-value');
    /* $('#customer').val("customer_id");*/
    // alert(customername);
  });

  $("#SaveButton").click(function Order()//Function to Select Product And Quantity
    { 
      var vendor_id = $("#vendor").val();
          
          var purchase_detail_data = [];
          var purchase_data = []; 
          
          // $(this).attr("disabled", true);
          var inv = {

            'vendor_id' : vendor_id,
            'amount'      : $("#TotalAmount1").val(),
            'paid'        : 0,
            'discount' : 1,
            'balance'     : $("#TotalAmount1").val(),
            'total'       : $("#TotalAmount1").val(),
          };
              purchase_data.push(inv);

             $('#order_table tr').each(function(row,tr){

              if($(tr).find('td:eq(0)').text() == "")
              {

              }
              else
              {
                  var details = 
                  {
                   'product_id' : $(tr).find('td:eq(0)').text(),
                   'order_quantity' : $(tr).find('td:eq(2)').text(),
                   'unit_rate' : $(tr).find('td:eq(3)').text(),
                   'product_rate' : $(tr).find('td:eq(4)').text(),
                  };
                  purchase_detail_data.push(details);
              }
          });
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/purchase/store') }}",
                  method: 'POST',
                  data: {'purchase_data':purchase_data, 'purchase_detail_data':purchase_detail_data},
                  success: function(result){
                     window.location = '/purchase/view/' + result;
                    }});
          });
</script> 
@endsection