@extends('layouts.master')
@section('content')
<style>
  div.content-header{
    display: none;
  }
  /*this class is common in all page and we did not need it in invoice view. we have already header in it*/

</style>

<section class="content">
  <div class="container-fluid">
 <!-- Content Wrapper. Contains page content -->
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Invoice</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Invoice</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="callout callout-info">
              <h5><i class="fas fa-info"></i> Note:</h5>
             
            </div>


            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-step-forward"></i> Step Forward.
                    <small class="float-right">
                      Date: {{ $data["invoice"]["created_at"]->format('d-M-Y')}}
                    </small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>Step Forward<br></strong>
                    Address:<br>
                    Area: lahore<br>
                    Phone: (+92)123-1234567<br>
                    Email: info@mail.com
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <strong>{{ $data["customer"]["name"]}}</strong><br>
                    {{ $data["customer"]["address"]}}<br>
                    Area: {{ $data["customer"]["area_location"]}}<br>
                    CNIC: {{ $data["customer"]["cnic"]}}<br>
                    Package: {{ $data["customer"]["package"]}}
                  </address>
                </div>
                <!-- /.col-->
                <div class="col-sm-4 invoice-col">
              
                  <span>Invoice : {{ $data["invoice"]["id"] }}</span>                 
                  
                  <br><br>
                  <span>Order ID: {{ $data["invoice"]["id"] }}</span><br>
                  <span>Payment Due:</span> ---<br>
                  <span>Account:</span> ---
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Sr</th>
                        <th>Qty</th>
                        <th>Product</th>
                        <th>UnitRate</th>
                        <th>ProductRate</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($data["invoice_detail"] as $invoice_detail)
                      <tr>
                        <td>{{ $invoice_detail["id"] }}</td>
                        <td>{{ $invoice_detail["product_name"] }}</td><!-- name -->
                        <td>{{ $invoice_detail["order_quantity"] }}</td>
                        <td>{{ $invoice_detail["unit_rate"] }}</td>
                        <td>{{ $invoice_detail["product_rate"] }}</td>
                        
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot><tr><th></th><th></th><th></th><th></th><th>{{ $data["invoice"]["sub_amount"] }}</th></tr></tfoot>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                  <p class="lead">Payment Methods:</p>
                  <img src="../../dist/img/credit/visa.png" alt="Visa">
                  <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                  <img src="../../dist/img/credit/american-express.png" alt="American Express">
                  <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

                  <!-- <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                    plugg
                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                  </p> -->
                </div>
                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">Amount Due: {{ $data["invoice"]["balance"]}}</p>

                  <div class="table-responsive">
                    <table class="table">
                      <!--  -->
                      <tr>
                        <th>Discount:</th>
                        <td>{{ $data["invoice"]["discount"]}}</td>
                      </tr>
                      <tr>
                        <th style="width:50%">Amount:</th>
                        <td>{{ $data["invoice"]["total"] }}</td>
                      </tr>
                     
                      <!-- <tr>
                        <th>Amount</th>
                        <td>{{ $data["invoice"]["total"] }}</td>
                      </tr> -->
                      <tr>
                        <th>Paid:</th>
                        <td>{{ $data["invoice"]["paid"] }}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                  <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Submit
                    Payment
                  </button>
                  <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</section>
  <!-- /.content-wrapper -->
@endsection