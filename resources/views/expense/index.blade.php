@extends('layouts.master')
@extends('layouts.inputfield')
@section('content') 

<section class="content">
  <div class="container-fluid"> 
    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Expense</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
           <div class="card-body">
            <form method="POST" action="{{route('expense_store')}}">
            @csrf                             
                <div class="col-md-11">
                    <div class="form-group float-left col-md-3">
                      <input type="text" class="form-control" name="expense" placeholder="Expense" min="0" required>
                    </div>
                    <div class="form-group float-left col-md-3">
                      <input type="text" class="form-control" name="description" placeholder="Description" required>
                    </div>
                    <div class="form-group float-left col-md-3">
                      <input type="number" class="form-control" name="price" placeholder="price" value="" min="0" required>
                    </div>
                </div>
                <div class="col ml-1">
                    <div class="form-group">
                      <button type="submit" id="AddButton" class="btn btn-primary float-left">Submit
                      </button>
                    </div>                  
                </div>
               </form> 
              </div>
          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Expenses</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="expense_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Expense</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Action</th>     
                  </tr>
                </thead>
                <tbody>
                  @foreach($data["expenses"] as $expenses)
                  <tr>
                    <td>{{$expenses->expense}}</td>
                    <td>{{$expenses->description}}</td>
                    <td>{{$expenses->price}}</td>                 
                    <td>
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_function({{$expenses->id}})">
                        Edit
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Expense</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

   <div class="modal fade" id="EditModal-default">
      <form method="POST" action="{{route('expense_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="Modal_title">Edit Expense</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 text-center">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Expense</label>
                    <input type="text" class="form-control col-md-8" id="expense" name="expense" placeholder="Expense">
                  </div>
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Description</label>
                    <input type="text" class="form-control col-md-8" id="description" name="description" placeholder="Description">
                  </div>
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Price</label>
                    <input type="number" class="form-control col-md-8" id="price" name="price" placeholder="Price" min="0">
                  </div>
                  
                  <!-- /.form-group -->
                </div>
              </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"> Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')
<script>
function edit_function(id)
{
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/expense/edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                     console.log(result);
                     //alert(result.expense);
                    
                    var expense = result.expense;                     
                     $("#expense").val(expense);

                     var description = result.description                     
                     $("#description").val(description);

                     var price = result.price;                     
                     $("#price").val(price);
                  }});
}
 
</script>
<script>
  $(function () {
    $("#expense_table").DataTable();
  });
</script>
@endsection