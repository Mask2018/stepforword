@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
  <style>
    .select2-container{ 
      display: inline-block;
      min-width: 41.66%;
    }
    .select2-container--default .select2-selection--single{
      height: auto;
      padding: 3px;
      border: transparent;
      border-bottom: 1px solid #ced4da;
    }
    .select2-selection.select2-selection--multiple{
      border-color: white;
      border-bottom: 1px solid #ced4da;
    }

    .select2-container--default .select2-selection--multiple
     .select2-selection__choice{
      color: black;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-search.select2-search--inline .select2-search__field{
      margin-bottom: 6px;
    }

    .select2-container--default.select2-container--focus 
    .select2-selection--multiple{
      border-color: white;
      border-bottom:1px solid #ced4da;
    }

  </style>

<section class="content">
  <div class="container-fluid"> 
    <div class="card collapsed-card"> 
          <div class="card-header">
            <h3 class="card-title">Add DP</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('dp_store')}}">
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3" style="clear: both;float: left;">Dp Number</label>
                    <input type="text" name="dp_number" class="form-control col-5" placeholder="DP Number" required>
                  </div>
                </div>
                  <!-- /.form-group -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Location</label>
                    <input type="text" class="form-control col-5" name="location" placeholder="location" required>
                  </div>
                </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Address/Street</label>
                    <input type="text" class="form-control col-5" name="address" placeholder="Addres & Street" required>
                  </div>
                </div>
                  <!-- /.form-group -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Description</label>
                    <input type="text" class="form-control col-5" name="description" placeholder="Description" required>
                  </div>
                </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Previous DP</label>
                    <select class="form-control select2" id="dpmanagment_previous" name="previous_dp">
                    <option value="" >Select Previous DP</option>
                    @foreach($data["dpmanagments"] as $dpmanagments) <!-- model name customer -->
                    <option value="{{ $dpmanagments->dp_number }}" data-name="{{ $dpmanagments->dp_number }}" >{{ $dpmanagments->dp_number }}</option>
                    @endforeach
                  </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->          
                <div class="col-md-6">
                  <div class="form-group">                
                  <label class="col-md-3" style="clear: both;float: left;">Next DP
                  </label>
                  <select class="form-control select2"
                      id="dpmanagment_next" name="next_dp[]" multiple="multiple" data-placeholder="Select Next DP">
                    <option value="">Select Next DP</option>
                    @foreach($data["dpmanagments"] as $dpmanagments) model name customer
                    <option value="{{ $dpmanagments->dp_number }}">{{ $dpmanagments->dp_number }}</option>
                    @endforeach
                  </select>
                    
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>


              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Device Serial</label>
                    <select class="form-control select2" id="device_number" name="device_serial">
                    <option value="">Select Device</option>
                    @foreach($data["devices"] as $devices) model name customer
                    <option value="{{ $devices->serial_number }}" data-name="{{ $devices->serial_number }}">{{ $devices->serial_number }}</option>
                    @endforeach
                  </select>
                    <!-- <input type="text" class="form-control col-md-4" name="device_serial" placeholder="Device Serial"> -->
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Created By</label>
                    <input type="text" class="form-control col-5" name="created_by" placeholder="Created By" required>
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>
              <!-- /.row -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">All DPs</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="dp" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>DP#</th>
                  <th>Device#</th>
                  <th>Address</th>
                  <th>Previous DP</th>
                  <th>Next DP</th>
                  <th>Action</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($data["dpmanagments"] as $dpmanagments)
                <tr>
                  <td>{{$dpmanagments->dp_number}}</td>
                  <td>{{$dpmanagments->device_serial}}</td>
                  <td>{{$dpmanagments->address}}</td>
                  <td>{{$dpmanagments->previous_dp}}</td>
                  <td>{{$dpmanagments->next_dp}}</td>
                  <td>
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_dp({{$dpmanagments->id}})">
                        Edit
                      </button>
                  </td>
                </tr>
                
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>DP#</th>
                  <th>Device#</th>
                  <th>Address</th>
                  <th>Previous DP</th>
                  <th>Next DP</th>
                  <th>Action</th> 
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

    <div class="modal fade" id="EditModal-default">
      <form method="POST" action="{{route('dp_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="Modal_title">Edit DP</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">DP#</label>
                  <input type="text" class="form-control col-md-8" id="dp_number" name="dp_number" placeholder="DP#">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Location</label>
                  <input type="text" class="form-control col-md-8" id="location" name="location" placeholder="Location">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Street/Address</label>
                  <input type="text" class="form-control col-md-8" id="address" name="address" placeholder="Street/Address">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Description</label>
                  <input type="text" class="form-control col-md-8" id="description" name="description" placeholder="Description">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Previous DP</label>
                  <input type="text" class="form-control col-md-8" id="previous_dp" name="previous_dp" placeholder="Previous DP">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Next DP</label>
                  <input type="text" class="form-control col-md-8" id="next_dp" name="next_dp" placeholder="Next DP">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Device Serial</label>
                  <input type="text" class="form-control col-md-8" id="device_serial" name="device_serial" placeholder="Device Serial">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Created By</label>
                  <input type="text" class="form-control col-md-8" id="created_by" name="created_by" placeholder="Created By">
                </div>
                
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"> Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

   /* $('#previous_dp').on('change', function (e) {
    }*/

  }); 

function edit_dp(id)
{
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/dp/edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var dp_number = result.dp_number;                     
                     $("#dp_number").val(dp_number);

                     var location = result.location;                     
                     $("#location").val(location);

                      var description = result.description;    
                     $("#description").val(description);

                       var previous_dp = result.previous_dp;    
                     $("#previous_dp").val(previous_dp);

                       var next_dp = result.next_dp;    
                     $("#next_dp").val(next_dp);

                       var device_serial = result.device_serial;    
                     $("#device_serial").val(device_serial);

                     var address = result.address;                     
                     $("#address").val(address);

                     var created_by = result.created_by; 
                     $("#created_by").val(created_by);
                  }});
}

</script>
<script>
  $(function () {
    $("#dp").DataTable();
  });
</script>
@endsection