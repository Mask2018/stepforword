@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
 
 <style>
    .select2-container{ 
      display: inline-block;
      min-width: 34%;
    }
    .select2-container--default .select2-selection--single{
      height: auto;
      padding: 3px;
      border: transparent;
      border-bottom: 1px solid #ced4da;
    }
    .form-control{
      display: initial;
    } 
    .col1_width{
      max-width: 9%;
    }

/* This is to remove the arrow of select element in IE */
select::-ms-expand {  display: none; }
select{
    -webkit-appearance: none;
    appearance: none;
}
@-moz-document url-prefix(){
  .ui-select{border: 1px solid #CCC; border-radius: 4px; box-sizing: border-box; position: relative; overflow: hidden;}
  .ui-select select { width: 110%; background-position: right 30px center !important; border: none !important;}
}
  </style>


<section class="content">
  <div class="container-fluid">

    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Package</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('package_store')}}">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-4">Package Name</label>
                  <input type="text" name="name" class="form-control col-md-4" placeholder="Package Name" required>
                </div>
              </div>
                <!-- /.form-group --> 
              <div class="col-md-6">
                <div class="form-group">
                   <label class="col-4">Upload</label>
                  <input type="number" class="form-control col-3" name="upload_speed" placeholder="speed" required>
                  <select class="form-control col-1 col1_width" name="upload_unit">
                    <option>KB</option>
                    <option>MB</option>
                    <option>GB</option>
                  </select>                  
                </div>
              </div>
                <!-- /.form-group -->
            </div>

              <!-- /.col -->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-4">Download</label>
                  <input type="number" class="form-control col-3" name="download_speed" placeholder="speed" min="0" required>
                  <select class="form-control col-1 col1_width" name="download_unit">
                    <option>KB</option>
                    <option>MB</option>
                    <option>GB</option>
                  </select>
                </div>
              </div>
                <!-- /.form-group -->
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-4">Rate</label>
                  <input type="number" class="form-control col-md-4" name="rate" placeholder="Rate" min="0" required>
                  
                </div>
              </div>
                <!-- /.form-group -->
            </div>
            
              <!-- /.col -->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Vendor Name</label>
                   <select class="form-control col-4 select2" name="vendor" required>
                    <option value="">Select Vendor</option>
                    @foreach($data["vendors"] as $vendors) <!-- model name customer -->
                    <option value="{{ $vendors->name }}" data-name="{{ $vendors->name }}">{{ $vendors->name }}</option>
                    @endforeach
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

          </div>
          <!-- /.card-body -->
    </div>

<div class="card">
            <div class="card-header">
              <h3 class="card-title">All Package</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tablebody" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Upload Speed</th>
                  <th>Download Speed</th>
                  <th>Rate</th>
                  <th>Action</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($data["packages"] as $packages)

                <tr>
                  <td>{{$packages->name}}</td>
                  <td>{{$packages->upload_speed}}{{$packages->upload_unit}}</td>
                  <td>{{$packages->download_speed}}{{$packages->download_unit}}</td>
                  <td>{{$packages->rate}}</td>                  
                  <td>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal" onclick="edit_Action({{$packages->id}})"> Edit
                    </button>                   
                  </td>
                </tr>

                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Upload Speed</th>
                  <th>Download Speed</th>
                  <th>Rate</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div> 
</div>
</section>

    <div class="modal fade" id="EditModal">
      <form method="POST" action="{{ route('package_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="Modal_title">Edit Package</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Name</label>
                  <input type="text" class="form-control col-7" id="name" name="name" placeholder="name">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Upload Speed</label>
                  <input type="text" class="form-control col-3" id="upload_speed" name="upload_speed" placeholder="Upload Speed">
                  <input type="text" class="form-control col-4" id="upload_unit" name="upload_unit">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Download Speed</label>
                  <input type="text" class="form-control col-3" id="download_speed" name="download_speed" placeholder="Download Speed">
                  <input type="text" class="form-control col-4" id="download_unit" name="download_unit">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Rate</label>
                  <input type="text" class="form-control col-7" id="rate" name="rate" placeholder="Rate">
                </div>
              
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Vendor</label>
                  <input type="text" class="form-control col-7" id="vendor_name" name="vendor" placeholder="Rate">

                  <!-- <select class="form-control col-4 select2" name="vendor" id="vendor">
                    <option value="">Select Vendor</option>
                    @foreach($data["vendors"] as $vendors) model name customer
                    <option value="{{ $vendors->name }}" data-name="{{ $vendors->name }}">{{ $vendors->name }}</option>
                    @endforeach
                  </select> -->
                </div>
                              
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"> Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')


<script>
  $(function () {
    $('.select2').select2()
  }) 
</script>

<script>
function edit_Action(id) //db view only
{
  //alert(id);
    //$('#your_form').attr('action', 'http://uri-for-button1.com');
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/package/edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var name = result.name;                     
                     $("#name").val(name);

                     var upload_speed = result.upload_speed;                  
                     $("#upload_speed").val(upload_speed);

                    var upload_unit = result.upload_unit;                  
                     $("#upload_unit").val(upload_unit);

                     var download_speed = result.download_speed; 
                     $("#download_speed").val(download_speed);

                     var download_unit = result.download_unit; 
                     $("#download_unit").val(download_unit);

                     var rate = result.rate;                     
                     $("#rate").val(rate);

                     var vendor = result.vendor;
                     $("#vendor_name").val(vendor);
                    //console.log(result.rgo);
                   }});
}
</script>
<script>
  $(function () {
    $("#tablebody").DataTable();
  });
</script>
@endsection