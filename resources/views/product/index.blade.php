@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
<style>
  .select2-container{ 
      display: inline-block;
      min-width: 41.66%;
    }

  .select2-container--default .select2-selection--single{
      height: auto;
      padding: 3px;
      border: transparent;
      border-bottom: 1px solid #ced4da;
    }
</style>

<section class="content">
  <div class="container-fluid">
@if(count ($errors) >0)
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
@endif

    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Product</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('product_store')}}">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3" style="clear: both;float: left;">Name</label>
                  <input type="text" name="name" class="form-control col-md-4" placeholder="Product Name" required>
                </div>
              </div>
                <!-- /.form-group --> 
              <div class="col-md-6">
                <div class="form-group">
                   <label class="col-md-3"style="clear: both;float: left;"> Category</label>
                  <!--<input type="text" class="form-control col-md-4" name="category" placeholder="category"> -->
                  <select name="category" id="product_categy" class="form-control select2 col-md-4">
                    <option value="device">Device</option>
                    <option value="dp">DP</option>
                    <option value="other">Other</option>
                  </select>
                </div>
              </div>
                <!-- /.form-group -->
            </div>
              <!-- /.col -->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Sale Price</label>
                  <input type="number" class="form-control col-md-4" name="sale_price" placeholder="Sale Price" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Purchase Price</label>
                  <input type="number" class="form-control col-md-4" name="purchase_price" placeholder="purchase Price" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
            </div>
              <!-- /.col -->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Quantity</label>
                  <input type="number" class="form-control col-md-4" name="quantity" placeholder="Quantity" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Sold</label>
                  <input type="number" class="form-control col-md-4" name="sold" placeholder="sold" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
            </div>
              <!-- /.col -->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Stock</label>
                  <input type="number" class="form-control col-md-4" name="stock" placeholder="Stock" min="0" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

          </div>
          <!-- /.card-body -->
    </div>

<div class="card">
            <div class="card-header">
              <h3 class="card-title">All Products</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="product" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Quantity</th>
                  <th>Sold</th>
                  <th>Action</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($data["products"] as $products)

                <tr>
                  <td>{{$products->name}}</td>
                  <td>{{$products->category}}</td>
                  <td>{{$products->quantity}}</td>
                  <td>{{$products->sold}}</td>                  
                  <td>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal" onclick="edit_product({{$products->id}})"> Edit
                    </button>                   
                  </td>
                </tr>

                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Quantity(s)</th>
                  <th>Sold</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div> 
</div>
</section>

    <div class="modal fade" id="EditModal">
      <form method="POST" action="{{ route('product_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="productModal_title">Edit Product</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Name</label>
                  <input type="text" class="form-control col-7" id="name" name="name" placeholder="name">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">category</label>
                  <input type="text" class="form-control col-7" id="category" name="category" placeholder="Category">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Sale Price</label>
                  <input type="text" class="form-control col-7" id="sale_price" name="sale_price" placeholder="Sale Price">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Purchase Price</label>
                  <input type="text" class="form-control col-7" id="purchase_price" name="purchase_price" placeholder="Purchase Price">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Quantity</label>
                  <input type="text" class="form-control col-7" id="quantity" name="quantity" placeholder="Quantity">
                </div>
                 <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Sold</label>
                  <input type="text" class="form-control col-7" id="sold" name="sold" placeholder="Sold">
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Stock</label>
                  <input type="text" class="form-control col-7" id="stock" name="stock" placeholder="Stock">
                </div>
                              
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"> Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
function edit_product(id) //db view only
{
  //alert(id);
    //$('#your_form').attr('action', 'http://uri-for-button1.com');
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/product/edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var name = result.name;                     
                     $("#name").val(name);

                     var category = result.category;                     
                     $("#category").val(category);

                     var quantity = result.quantity;                     
                     $("#quantity").val(quantity);

                     var sold = result.sold;                     
                     $("#sold").val(sold);

                     var stock = result.stock;                     
                     $("#stock").val(stock);

                     var sale_price = result.sale_price;                     
                     $("#sale_price").val(sale_price);

                     var purchase_price = result.purchase_price;
                     $("#purchase_price").val(purchase_price);
                    //console.log(result.rgo);
                    // console.log(regional_offices);
                     /*$('#adduserpopup .modal-header').children('h4').text('Update User');
                    $('#adduserpopup').modal('show');*/
                  }});
}
</script>
<script>
  $(function () {
    $("#product").DataTable();
  });
</script>
@endsection