<style>
  button{
    cursor: pointer;
  }
  textarea.form-control {
    min-height: 35px;
    height: 37px;
    max-height: 38px;
  }
  
  .modal-footer button.btn-primary{
    pointer-events: none;
  }
  button.Actionbtn{
    padding: 5px;
    letter-spacing: 1.2px;
    margin-right: 2px;
    background-color: transparent;
    border-top: none;
     border-color: grey;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;   
    border-bottom: 1px solid grey;   
  }
    button.Actionbtn:hover{
      border-bottom: 1px solid transparent;
      border-left: 1px solid coral;
      border-right: 1px solid coral;
    }
  .textarea.form-control {
    min-height: 37px;
    height: 37px;
  }
  input{
    border:1px solid #ced4da;
  }
  input.form-control,textarea.form-control,select.form-control{
    border:transparent;
    border-bottom: 1px solid #ced4da;
  }
/*  .select2-container--default .select2-selection--single{
    border:none;
    border-bottom: 1px solid #ced4da;
  }
  .select2 .select2-container .select2-container--default .select2-container--focus{
    display: inline-block;
  }*/

  .select2-container{ 
      display: inline-block;
      min-width: 33.336%;
    }
    .select2-container--default .select2-selection--single{
      height: auto;
      padding: 3px;
      border: transparent;
      border-bottom: 1px solid #ced4da;
    }
    .select2-selection.select2-selection--multiple{
      border-color: white;
      border-bottom: 1px solid #ced4da;
    }

    .select2-container--default .select2-selection--multiple
     .select2-selection__choice{
      color: black;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-search.select2-search--inline .select2-search__field{
      margin-bottom: 6px;
    }

    .select2-container--default.select2-container--focus 
    .select2-selection--multiple{
      border-color: white;
      border-bottom:1px solid #ced4da;
    }
.card-body label{
  margin-bottom: 0px;
  padding: 1%;
}

  ::placeholder , ::-ms-input-placeholder{
    color:rgb(118, 118, 118)
  }
</style>

