<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link">
      <img src="/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="{{ route('home') }}" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              <!--   <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>
           <!--  <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('home') }}" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
            </ul> -->
          </li>
          <!-- <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Widgets
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li> -->
          <!-- invoice -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Invoice
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('invoice')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sale Invoices
                    </p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{route('purchase')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Purchase Invoices
                    </p>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked Invoices</p> <!-- <span class="right badge badge-danger">4</span> </p>->
                </a>
              </li> -->
            </ul>
          </li>
          <!-- invoice close -->


          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Customers
                <i class="fas fa-angle-left right"></i>
                <!-- 
                <span class="badge badge-info right">6</span>
               -->
              </p> 
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('customer')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Customers
                    </p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{route('received_receipt')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Received Receipt</p>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked Customers</p>
                </a>
              </li> -->
            </ul>
          </li>
   
        

         <!-- vandor navbar -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Vendor
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
             <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('vendor')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Vendors
                    </p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{route('paid_receipt')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Paid Receipt</p>
                </a>
              </li>
             <!-- <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked Vendors</p>
                </a>
              </li>-->
            </ul> 
          </li>
          <!-- vendor close -->


                      <!-- ShareHolder navbar -->
          <li class="nav-item has-treeview">
            <a href="{{route('shareholder')}}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                ShareHolder
                <!-- <i class="fas fa-angle-left right"></i> -->
              </p>
            </a>
           <!--  <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('shareholder')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All ShareHolders
                    </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked ShareHolders</p>
                </a>
              </li>
            </ul> -->
          </li>
          <!-- ShareHolder close -->




          <!-- client request -->

          <li class="nav-item has-treeview">
            <a href="{{route('clientrequest')}}" class="nav-link">
              <i class="nav-icon fa fa-registered"></i>
              <p>
                Request Form
                <!-- <i class="fas fa-angle-left right"></i> -->
                <!-- 
                <span class="badge badge-info right">6</span>
               -->
              </p> 
            </a>     
          </li>
        
         <!-- close -->

         <!-- product -->


          <li class="nav-item has-treeview">
            <a href="{{route('product')}}" class="nav-link">
              <i class="nav-icon fas fa fa-qrcode" aria-hidden="true"></i>
              <p>
                Products
                <!-- <i class="fas fa-angle-left right"></i> -->
              </p>
            </a>
            <!--<ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('product')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Products
                    </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked Products</p>
                </a>
              </li>
            </ul> -->
          </li>
            <!-- product close -->

          <li class="nav-item has-treeview">
            <a href="{{route('package')}}" class="nav-link">
              <i class="nav-icon fas fa fa-qrcode" aria-hidden="true"></i>
              <p>
                Package
                <!-- <i class="fas fa-angle-left right"></i> -->
              </p>
            </a>
            <!-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('package')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Packages
                    </p>
                </a>
              </li>
              
            </ul> -->
          </li>

          
          <!-- package close -->

          <li class="nav-item has-treeview">
            <a href="{{route('complain')}}" class="nav-link">
              <i class="nav-icon fa fa-comments"></i>
              <p>
                Complains
                <!-- <i class="fas fa-angle-left right"></i> -->
              </p>
            </a>
            <!-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('complain')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Complains
                    </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked Complain</p>
                </a>
              </li>
            </ul> -->
          </li>
          <!-- complain navbar close -->





           <!-- device -->
          <li class="nav-item has-treeview">
            <a href="{{route('device')}}" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Router
               <!--  <i class="fas fa-angle-left right"></i> -->
              </p>
            </a>
           <!--  <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('device')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ALL Router
                    </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked Router</p>
                </a>
              </li>
            </ul> -->
          </li>
          <!-- devce close -->


          <!-- dp -->
          <li class="nav-item has-treeview">
            <a href="{{route('dp')}}" class="nav-link">
              <i class="nav-icon fas fa-link"></i>
              <p>
                DP
                <!-- <i class="fas fa-angle-left right"></i> -->
              </p>
            </a>
            <!-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('dp')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ALL DP
                    </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked DP</p>
                </a>
              </li>
            </ul> -->
          </li>
          <!-- dp close -->

          <!-- expense --
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-print"></i>
              <p>
                Receipt
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('paid_receipt')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Paid Receipt
                    </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('received_receipt')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Received Receipt</p>
                </a>
              </li>
            </ul>
          </li>
          <!-- expense close -->

          <!-- expense -->
          <li class="nav-item has-treeview">
            <a href="{{route('expense')}}" class="nav-link">
              <i class="nav-icon fas fa-credit-card"></i>
              <p>
                Expense
                <!-- <i class="fas fa-angle-left right"></i> -->
              </p>
            </a>
            <!-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('expense')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ALL Expense
                    </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked Expense</p>
                </a>
              </li>
            </ul> -->
          </li>
          <!-- expense close -->

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
