<style type="text/css">
  .card-header{
    background: #343a40;
    color: #adb5bd;
  }
  .dropdown .dropdown-item{
    padding:0.5rem 1rem;
  }
  #newpadding{

  }
</style>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav col-6">
      <li class="nav-item mt-1">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>  
 
  <ul class="navbar-nav col-6">
       <!-- SEARCH FORM -->
    <form class="form-inline col-10" style="margin-bottom: initial;">
      <div class="input-group input-group-sm" style="width: inherit;">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>     
       <li class="nav-item dropdown">                                 
            <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                                
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->