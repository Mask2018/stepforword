@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
<style>
  textarea.form-control {
    min-height: 35px;
    height: 37px;
    max-height: 38px;
  }  
</style>

<section class="content">
  <div class="container-fluid">

    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Vendor</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('vendor_store')}}">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3" style="clear: both;float: left;">Name</label>
                  <input type="text" name="name" class="form-control col-md-4" placeholder="Vendor Name" required>
                </div>
              </div>
                <!-- /.form-group -->
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;"> CNIC</label>
                  <input type="text" class="form-control col-md-4" name="cnic" placeholder="#####-########-#" pattern="\d{5}-\d{8}-\d{1}" title="CNIC Format Required" required>
                </div>
              </div>
                <!-- /.form-group -->
              </div> 
              <!-- /.col -->
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Area</label>
                  <input type="text" class="form-control col-md-4" name="area_location" placeholder="Area Location" required>
                </div>
              </div>
                <!-- /.form-group -->
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Address</label>
                  <input type="text" class="form-control col-md-4" name="address" placeholder="address" required>
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Credit</label>
                  <input type="number" class="form-control col-md-4" name="credit" placeholder="credit" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Balance</label>
                  <input type="number" class="form-control col-md-4" name="balance" placeholder="balance" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Sale</label>
                  <input type="number" class="form-control col-md-4" name="sale" placeholder="sale" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Purchase</label>
                  <input type="number" class="form-control col-md-4" name="purchase" placeholder="purchase" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <!-- /.col -->
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Package</label>
                  <input type="text" class="form-control col-md-4" name="package" placeholder="package" required>
                </div>
              </div>
                <!-- /.form-group -->
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Expenses</label>
                  <input type="number" class="form-control col-md-4" name="expenses" placeholder="expenses" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Description</label>
                  <textarea type="text" class="form-control col-md-4"placeholder="Description" name="desciption" required></textarea>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <div class="input-group">
                      <div class="custom-file col-6">
                        <input type="file" class="upload form-control" accept="image/*" name="file" multiple />
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">1 CNIC</span>
                      </div>
                  </div>
                  <div class="input-group">
                      <div class="custom-file col-6">
                        <input type="file" class="upload form-control" accept="image/*" name="file_second" multiple />
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">2 CNIC</span>
                      </div>
                  </div>
                </div> 
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

            </div>
            <!-- /.row -->
            <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

          </div>
          <!-- /.card-body -->
    </div>

<div class="card">
            <div class="card-header">
              <h3 class="card-title">All Vendors</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="vendor" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Address</th>
                  <th>PackageName</th>
                  <th>Balance</th>
                  <th>Action</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($data["vendors"] as $vendors)
                <tr>
                  <td><a href="{{route('vendor_ledger',$vendors->id)}}">{{$vendors->name}}</a></td>
                  <td>{{$vendors->address}}</td>
                  <td>{{$vendors->package}}</td>
                  <td>{{$vendors->balance}}</td>                  
                  <td>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-Vendor" onclick="edit_vendor({{$vendors->id}})"> Edit
                    </button>                   
                  </td>
                </tr>

                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>


</div>
</section>

    <div class="modal fade" id="EditModal-Vendor">
      <form method="POST" action="{{ route('vendor_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="vendorModal_title">Edit Vendor</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Name</label>
                  <input type="text" class="form-control col-md-8" id="name" name="name" placeholder="name">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">CNIC</label>
                  <input type="text" class="form-control col-md-8" id="cnic" name="cnic" placeholder="cnic">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Sale</label>
                  <input type="number" class="form-control col-md-8" id="sale" name="sale" placeholder="sale">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Purchase</label>
                  <input type="number" class="form-control col-md-8" id="purchase" name="purchase" placeholder="purchase">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Balance</label>
                  <input type="number" class="form-control col-md-8" id="balance" name="balance" placeholder="Balance">
                </div>
                 <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Credit</label>
                  <input type="number" class="form-control col-md-8" id="credit" name="credit" placeholder="Credit">
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Address</label>
                  <input type="text" class="form-control col-md-8" id="address" name="address" placeholder="Address">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Package</label>
                  <input type="text" class="form-control col-md-8" id="package" name="package" placeholder="Package">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Expenses</label>
                  <input type="number" class="form-control col-md-8" id="expenses" name="expenses" placeholder="expenses">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Area</label>
                  <input type="text" class="form-control col-md-8" name="area_location" id="area_location" placeholder="Area Location">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Description</label>
                  <input type="text" class="form-control col-md-8" id="desciption" name="desciption" placeholder="desciption">
                </div>                
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"> Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
      </div>
@endsection
@section('content_js')

<script>
function edit_vendor(id)
{
  //alert(id);
    //$('#your_form').attr('action', 'http://uri-for-button1.com');
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/vendor/edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var name = result.name;                     
                     $("#name").val(name);

                     var address = result.address;                     
                     $("#address").val(address);

                     var package = result.package;                     
                     $("#package").val(package);

                     var cnic = result.cnic;                     
                     $("#cnic").val(cnic);

                     var balance = result.balance;                     
                     $("#balance").val(balance);

                     var sale = result.sale;                     
                     $("#sale").val(sale);

                     var purchase = result.purchase;                     
                     $("#purchase").val(purchase);
                     
                     var area_location = result.area_location;              
                     $("#area_location").val(area_location);

                     var expenses = result.expenses;                     
                     $("#expenses").val(expenses);

                     var credit = result.credit;                     
                     $("#credit").val(credit);

                     var desciption = result.desciption;                     
                     $("#desciption").val(desciption);
                    //console.log(result.rgo);
                    // console.log(regional_offices);
                     /*$('#adduserpopup .modal-header').children('h4').text('Update User');
                    $('#adduserpopup').modal('show');*/
                  }});
} 

</script>
<script>
  $(function () {
    $("#vendor").DataTable();
  });
</script>
@endsection