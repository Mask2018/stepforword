@extends('layouts.master')
@section('content')
<style>
</style> 
<section class="content">
  <div class="container-fluid"> 
    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vendor Legder</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="customerledger" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Number</th>
                    <th>Debit</th>
                    <th>Credit</th> 
                    <th>Balance</th>                 
                  </tr>
                </thead>
                <tbody>
                  @foreach($data["vendor_ledgers"] as $vendor_ledgers)
                  <tr>
                    <td>{{$vendor_ledgers->debit}}</a></td>
                    <td>{{$vendor_ledgers->debit}}</td>
                    <td>{{$vendor_ledgers->debit}}</td>
                    <td>{{$vendor_ledgers->credit}}</td>
                    <td>{{$vendor_ledgers->balance}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

@endsection
@section('content_js')

@endsection