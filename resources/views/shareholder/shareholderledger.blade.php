@extends('layouts.master')
@section('content')
<style>
</style> 
<section class="content">
  <div class="container-fluid"> 
    <div class="card">
            <div class="card-header">
              <h3 class="card-title">ShareHolder Legder</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="shareholderledger" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Number</th>
                    <th>Debit</th>
                    <th>Credit</th> 
                    <th>Balance</th>                 
                  </tr>
                </thead>
                <tbody>
                  @foreach($data["share_holder_ledgers"] as $share_holder_ledgers)
                  <tr>
                    <td>{{$share_holder_ledgers->debit}}</a></td>
                    <td>{{$share_holder_ledgers->debit}}</td>
                    <td>{{$share_holder_ledgers->debit}}</td>
                    <td>{{$share_holder_ledgers->credit}}</td>
                    <td>{{$share_holder_ledgers->balance}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

@endsection
@section('content_js')

@endsection