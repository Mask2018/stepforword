@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
<style>
  .select2-container{ 
      display: inline-block;
      min-width: 41.66%;
    }

  .select2-container--default .select2-selection--single{
      height: auto;
      padding: 3px;
      border: transparent;
      border-bottom: 1px solid #ced4da;
    }
</style>
<section class="content">
  <div class="container-fluid"> 
    <div class="card collapsed-card"> 
          <div class="card-header">
            <h3 class="card-title">Add Receipt</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('paid_receipt_store')}}">
            @csrf                             
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3" style="clear: both;float: left;">Vendor Name</label>
                    <select class="form-control select2" id="vendor" name="vendor_id">
                          <option value="">Select Vendor</option>
                          @foreach($data["vendors"] as $vendors)
                          <option value="{{ $vendors->id }}" data-name="{{ $vendors->name }}">{{ $vendors->name }}</option>
                          @endforeach
                        </select>
                    <!--  <input type="text" name="vendor_name" class="form-control col-md-4" placeholder="Vendor Name"> -->
                  </div>
                </div>
                  <!-- /.form-group -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;"> Paid Amount</label>
                    <input type="number" class="form-control col-md-5" name="paid_amount" placeholder="Paid Amount" min="0" required>
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>
                  
                  <!-- /.form-group -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Cash /Cheque #</label>
                    <input type="text" class="form-control col-md-5" name="r_type" placeholder="Cash /Cheque#" required>
                  </div>
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Bank Name</label>
                    <input type="text" class="form-control col-md-5" name="bank_name" placeholder="Bank Name" required>
                  </div>
                </div>
                  <!-- /.form-group -->
              </div>
                <!-- /.col -->

              <div class="row">
                <div class="col-md-6">
                 
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Being</label>
                    <input type="text" class="form-control col-md-5" name="being" placeholder="Being" required>
                  </div>
                  <!-- /.form-group -->
                </div>
              <!-- /.col -->

              </div>
                <div class="col">
                    <div class="card-footer form-group">
                      <button type="submit" id="AddButton" class="btn btn-primary float-left">Submit
                      </button>
                    </div>                  
                </div>
            </form> 
          </div>
          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Paid Receipts</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="expense" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Vendor Name</th>
                    <th>Paid Amount</th>
                    <th>Bank Name</th>
                    <th>Cash/Cheque#</th>
                    <th>Being</th>
                    <th>Date</th>
                    <th>Action</th>     
                  </tr>
                </thead>
                <tbody> 
                  @foreach($data["paid_receipts"] as $paid_receipts)
                  <tr>
                    <td>{{$paid_receipts->vendor->name}}</td>
                    <td>{{$paid_receipts->paid_amount}}</td>
                    <td>{{$paid_receipts->bank_name}}</td>
                    <td>{{$paid_receipts->r_type}}</td>
                    <td>{{$paid_receipts->being}}</td>
                    <td>{{($paid_receipts->updated_at)->format('Y-m-d')}}</td>
                    <td>
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_function({{$paid_receipts->id}})">
                          Edit
                        </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>
    <div class="modal fade" id="EditModal-default">
      <form method="POST" action="{{route('device_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="Modal_title">Edit Receipt</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Paid Amount</label>
                  <input type="text" class="form-control col-md-8" id="paid_amount" name="paid_amount" placeholder="Paid Amount">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Being</label>
                  <input type="text" class="form-control col-md-8" id="being" name="being" placeholder="Being">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Bank /Cheque#</label>
                  <input type="text" class="form-control col-md-8" id="r_type" name="r_type" placeholder="Bank /cheque#">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Bank name</label>
                  <input type="text" class="form-control col-md-8" id="bank_name" name="bank_name" placeholder="Bank Name">
                </div>
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  }); 
</script>
<script>
function edit_function(id)
{
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/paid_receipt/edit') }}",
                  method: 'POST',
                  data: {
                    id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
              

                     var paid_amount = result.paid_amount;                     
                     $("#paid_amount").val(paid_amount);

                     var being = result.being;                     
                     $("#being").val(being);

                     var r_type = result.r_type;                     
                     $("#r_type").val(r_type);

                     var bank_name = result.bank_name;                     
                     $("#bank_name").val(bank_name);
                  }});
}

</script>
<script>
  $(function () {
    $("#tablebody").DataTable();
  });
</script>
@endsection