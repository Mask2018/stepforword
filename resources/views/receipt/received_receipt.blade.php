@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
  <style>
  .select2-container{ 
      display: inline-block;
      min-width: 41.66%;
    }

  .select2-container--default .select2-selection--single{
      height: auto;
      padding: 3px;
      border: transparent;
      border-bottom: 1px solid #ced4da;
    }
</style>
<section class="content">
  <div class="container-fluid"> 
    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Receipt</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('received_receipt_store')}}">
            @csrf                             
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3" style="clear: both;float: left;">Customer Name</label>
                    <select class="form-control select2" id="customer" name="customer_id">
                          <option value="">Select_Customer</option>
                          @foreach($data["customers"] as $customers)
                          <option value="{{ $customers->id }}" data-name="{{ $customers->name }}">{{ $customers->name }}</option>
                          @endforeach
                        </select>
                    <!-- <input type="text" name="customer_name" class="form-control col-md-4" placeholder="Customer Name"> -->
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Amount</label>
                    <input type="number" class="form-control col-md-5" name="received_amount" placeholder="Received Amount" min="0">
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>
                <!-- /.col -->

              <div class="row">
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Cash /Cheque #</label>
                    <input type="text" class="form-control col-md-5" name="r_type" placeholder="Cash /Cheque#">
                  </div>
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Bank Name</label>
                    <input type="text" class="form-control col-md-5" name="bank_name" placeholder="Bank Name">
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>

              <div class="row">
                <!-- /.col -->
                <div class="col-md-6">
                 
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Being</label>
                    <input type="text" class="form-control col-md-5" name="being" placeholder="Being">
                  </div>
                  <!-- /.form-group -->
                </div>
              <!-- /.col -->

              </div>
                <div class="col">
                    <div class="card-footer">
                      <button type="submit" id="AddButton" class="btn btn-primary float-left">Submit
                      </button>
                    </div>                  
                </div>
               </form> 
              </div>
          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Received Receipts</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
             <table id="expense" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Customer Name</th>
                    <th>Paid Amount</th>
                    <th>Bank Name</th>
                    <th>Cash/Cheque#</th>
                    <th>Being</th>
                    <th>Date</th>
                    <th>Action</th>     
                  </tr>
                </thead>
                <tbody>
                  @foreach($data["received_receipts"] as $received_receipts)
                  <tr>
                    <td>{{$received_receipts->customer->name}}</td>
                    <td>{{$received_receipts->received_amount}}</td>
                    <td>{{$received_receipts->bank_name}}</td>
                    <td>{{$received_receipts->r_type}}</td>
                    <td>{{$received_receipts->being}}</td>
                    <td>{{($received_receipts->updated_at)->format('Y-m-d')}}</td>
                    <td>
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_function({{$received_receipts->id}})">
                        Edit
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>
    <div class="modal fade" id="EditModal-default">
      <form method="POST" action="{{route('device_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="Modal_title">Edit Receipt</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Amount</label>
                  <input type="text" class="form-control col-md-8" id="received_amount" name="received_amount" placeholder="Received Amount">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Being</label>
                  <input type="text" class="form-control col-md-8" id="being" name="being" placeholder="Being">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Bank /Cheque#</label>
                  <input type="text" class="form-control col-md-8" id="r_type" name="r_type" placeholder="Bank /cheque#">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Bank name</label>
                  <input type="text" class="form-control col-md-8" id="bank_name" name="bank_name" placeholder="Bank Name">
                </div>
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')


<script>
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  }); 
</script>

<script>
function edit_function(id)
{
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/received_receipt/edit_r') }}",
                  method: 'POST',
                  data: {
                    id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var customer_name = result.customer_name;                     
                     $("#customer_name").val(customer_name);

                     var received_amount = result.received_amount;                     
                     $("#received_amount").val(received_amount);

                     var being = result.being;                     
                     $("#being").val(being);

                     var r_type = result.r_type;                     
                     $("#r_type").val(r_type);

                     var bank_name = result.bank_name;                     
                     $("#bank_name").val(bank_name);
                  }});
}
</script>
<script>
  $(function () {
    $("#expense").DataTable();
  });
</script>
@endsection