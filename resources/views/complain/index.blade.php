@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
<style> 
  .select2-container--default .select2-selection--single{
    border:transparent;
    height: 38px;
    border-bottom: 1px solid #ced4da;
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow b{
    top: auto;
    bottom: 20%;
  }
  address input ,h5 input{
    border:none;
    outline: none;
  }
  textarea.form-control {
    min-height: 36px;
    height: 37px;
    max-height: 38px;
  }  

@media (min-width: 576px){
  .modal-dialog {
      max-width: 55%;
  }
}
</style>

<section class="content">
  <div class="container-fluid"> 
    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Register Complain</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
 

          <div class="card-body">
            <form method="POST" action="{{route('complain_store')}}">
            @csrf
              <div class="row">
                <div class="col-md-8">
                    <div class="form-group float-left col-md-4">
                        <select class="form-control select2" id="customer" name="customer_id">
                          <option value="">Select_Customer</option>
                          @foreach($data["customers"] as $customers)
                          <option value="{{ $customers->id }}" data-name="{{ $customers->name }}">{{ $customers->name }}</option>
                          @endforeach
                        </select>
                    </div>

                    <div class="form-group float-left col-md-4">
                      <input type="text" class="form-control" id="title" name="title" placeholder="Title" required>
                    </div>
                    <div class="form-group float-left col-md-4">
                      <textarea type="text" class="form-control" name="desciption"  placeholder="Description" required></textarea>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                      <button type="submit" id="AddButton" class="btn btn-primary float-left">Submit
                      </button>
                    </div>                  
                </div>

              </div>
            </form> 
          </div>
          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Complains</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="complain" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Customer</th>
                  <th>Title</th>               
                  <th>Status</th>
                 <!--  <th>Assign To</th> -->
                </tr>
                </thead>
                <tbody>
                  @foreach($data["complains"] as $complains)
                  <tr>
                    <td>
                      <button type="button" class="btn btn-default" 
                        data-toggle="modal" data-target="#ComplainModal"
                        onclick="CompalinDetailView({{$complains->id}})">
                        {{$complains->id}}
                      </button>
                    </td>
                    <td>{{$complains->customer->name}}</td>
                    <td> {{$complains->title}}</td>              
                    <td> <span class="right badge badge-danger">
                          {{ ($complains->status == 0)?"New": "Resolve"}}</span></td>
                    
                    <!-- <td> {{$complains->assign_to}}</td> -->                
                  </tr>
                  @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
    </div>

  </div>
</section>
 

  <div class="modal fade" id="ComplainModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="ComplainModal_title">Complain Detail View</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">

                 <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h5>
                    <i class="fas fa-step-forward float-left"> Step Forward.</i>
                    <small class="float-right">
                        <input type="text" id="model_updated_at" name="date"><br> 
                    </small>
                  </h5>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info mt-2">
                <div class="col-sm-4 invoice-col">
                  <address>
                    <input type="text" id="model_name" name="name" style="outline: none;"><br>
                    <input type="text" id="model_area_location" name="area_location"><br>
                  </address>
                </div>

                <div class="col-sm-4 invoice-col">
                  <address>
                    <input type="text" id="model_title" name="title"><br>
                    <input type="text" id="model_desciption" name="desciption"><br>
                  </address>
                </div>
                <!-- / col -->
                <div class="col-sm-4 invoice-col">
                  <address>
                    <input type="text" id="model_address" name="address"><br>
                    <input type="text" id="model_remaks" name="remaks"><br>
                  </address>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
          

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <a href="#" target="_blank" class="btn btn-default float-right"><i class="fas fa-print"></i> Print</a>
                  
                </div>
              </div>
            </div>              
                <!-- /.form-group -->
              </div>
            </div>
            </div>           
          </div>
          <!-- /.modal-content -->
        </div>

        <!-- /.modal-dialog -->
  </div>
@endsection
@section('content_js')


<script>
$(function () {
    //Initialize Select2 Elements 
  $(".select2").select2({
    tags: true
  });

});
</script>


<script> 
function CompalinDetailView(id) //db view only
{
 // alert(id);
  
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/complain/edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                    console.log(result);
                  
                    var name = result["customer"].name;          
                     $("#model_name").val(name);

                     var title = result["complain"].title;                     
                     $("#model_title").val(title);

                     var desciption = result["complain"].desciption;                     
                     $("#model_desciption").val(desciption);

                     var assign_to = result["complain"].assign_to;                     
                     $("#model_assign_to").val(assign_to);

                     var remaks = result["complain"].remaks;                     
                     $("#model_remaks").val(remaks);

                      var date = result["complain"].updated_at;                     
                     $("#model_updated_at").val(date);

                     var area_location = result["customer"].area_location;                     
                     $("#model_area_location").val(area_location); 

                      var address = result["customer"].address;          
                     $("#model_address").val(address);                     
                  }});
}
 
  $(function () { 
    $("#complain").DataTable();
  });
</script>
@endsection