@extends('layouts.master')
@section('content')
<style>
</style> 
<section class="content">
  <div class="container-fluid"> 
    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Message Detail</h3>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              
              <table id="message_details" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Message</th>
                    <th>Date</th>                                     
                  </tr>
                </thead>
                <tbody>
                  @foreach($data["message_details"] as $message_details)
                  <tr>
                    <td>{{$message_details->message}}</td>
                    <td>{{$message_details->updated_at}}</td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Message</th>
                    <th>Date</th>                                     
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

@endsection
@section('content_js')

@endsection