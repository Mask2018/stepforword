@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
 
<section class="content">
  <div class="container-fluid">
    @if(count ($errors) >0)
            <div class="alert alert-danger">
              <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
              </ul>
            </div>
          @endif

    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Number</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('message_store')}}">
              @csrf
              <div class="row">
                <div class="col-md-10">                
                  <div class="col-md-4" style="float: left;clear: both;">
                      <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Name">
                      </div>
                  </div>
                  <div class="col-md-4 float-left">
                    <div class="form-group">
                      <input type="text" name="phone_number" class="form-control" placeholder="Phone Number">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  </div>
            
              <!-- /.row -->
              <div class="col-md-1 float-left">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
            </form>
          </div>
          <!-- /.card-body -->
    </div>

    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Send Message</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('message_send')}}">
              @csrf
              <div class="row">
                <div class="col-md-8">
                
                  <div class="form-group float-left col col-md-4">
                     <select class="select2 form-control" id="product">
                          <option value="">Select Name</option>
                          @foreach($data["messages"] as $messages)
                          <option value="{{ $messages->id }}" data-name="{{ $messages->name }}" data-price="{{ $messages->p_number }}" > {{ $messages->name }}</option>
                          @endforeach
                      </select>
                  </div>

                  <div class="col-md-4" style="float: left;clear: both;">
                      <div class="form-group">
                        <input type="text" class="form-control" name="message_id" placeholder="m_id">
                      </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="col-md-4 float-left">
                    <div class="form-group">
                      <input type="text" class="form-control" name="message" placeholder="Message">
                    </div>
                    <!-- /.form-group -->
                  </div>
                <!-- /.col -->
                </div>
            
              <!-- /.row -->
              <div class="col-md-1 float-left">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
            </form>
          </div>
          <!-- /.card-body -->
    </div>


    <div class="card">
            <div class="card-header">
              <h3 class="card-title">View Number</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tablebody" class="table table-bordered table-striped">
                <thead>
                <tr>                  
                  <th>Name</th>
                  <th>Number</th>
                  <th>Date</th>
                  <th>Action</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($data["messages"] as $messages)
                <tr>
                  <td><a href="{{route('message_detail',$messages->id)}}">{{$messages->name}}</td>
                  <td>{{$messages->phone_number}}</td>
                  <td>{{$messages->updated_at}}</td>
                  <td>
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_action({{$messages->id}})">
                        Edit
                      </button>
                  </td>
                </tr>
                
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Number</th>
                  <th>Date</th>
                  <th>Action</th> 
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

 <div class="modal fade" id="EditModal-default">
      <form method="POST" action="{{route('message_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="Modal_title">Edit Message</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Name</label>
                  <input type="text" class="form-control col-md-8" id="name" name="name" placeholder="Name">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Phone Number</label>
                  <input type="text" class="form-control col-md-8" id="phone_number" name="phone_number" placeholder="Phone Number">
                </div>
              
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
function edit_action(id)
{
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/message/edit') }}",
                  method: 'POST',
                  data: {
                    id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var name = result.name;                     
                     $("#name").val(name);

                     var phone_number = result.phone_number;                     
                     $("#phone_number").val(phone_number);
                  }});
}

</script>
<script>
  $(function () {
    $("#tableBody").DataTable();
  });
</script>
@endsection