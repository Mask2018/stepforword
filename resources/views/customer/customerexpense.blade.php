@extends('layouts.master')
@extends('layouts.inputfield')
@section('content') 
<style>
  textarea.form-control {
    min-height: 35px;
    height: 37px;
    max-height: 38px;
  }
  .select2-container--default .select2-selection--single{
    border:transparent;
    height: auto;
    padding-bottom: 2px;
    border-bottom: 1px solid #ced4da;
  }
  
  .select2-container {
    display: inline-block;
  }

</style>
<h2 class="ml-3"><a href="{{url('/customer')}}" style="color:#343a40"><i class="fas fa-user" style="font-weight: normal;"></i></a> {{$customer_name->name}}</h2>
<section class="content"> 
  <div class="container-fluid"> 
    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Expense</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('customer_expense_store')}}">
            @csrf
              <div class="row">
                <div class="col-md-6" style="display: none;">
                  <div class="form-group">
                    <label class="col-md-3" style="clear: both;float: left;">Customer Name</label>
                    <input type="text" name="customer_id" class="form-control col-md-4" placeholder="Customer id" value="{{$data['customer_id']}}" hidden>
                    
                  </div>
                </div>      
                <!-- /.col --> 
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Title</label>
                    <input type="text" class="form-control col-md-4" name="title" required>
                  </div>
                </div>
           
                  <!-- /.form-group -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Price</label>
                    <input type="number" class="form-control col-md-4" name="price" placeholder="0" required>
                  </div>
                </div>
                  <!-- /.form-group -->        
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Description</label>
                    <input type="text" class="form-control col-md-4" name="description" placeholder="description">
                  </div>
                </div>
                  <!-- /.form-group -->
              </div>
              <!-- /.col -->
                <!-- /.form-group -->
              <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit
                    </button>
              </div>
            </form> 
          </div>          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Expense Details</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tabel_body" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Price</th>
                  <th>Description</th>
                  <th>Action</th>                
                </tr>
                </thead>
                <tbody>
                @foreach($data["customer_expenses"] as $customer_expenses)
                <tr>                  
                  <td>{{$customer_expenses->title}}</td>
                  <td>{{$customer_expenses->price}}</td>
                  <td>{{$customer_expenses->description}}</td>                  
                  <td>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_record({{$customer_expenses->id}})">Edit</button>                 
                  </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Description</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

    <div class="modal fade" id="EditModal-default">
      <form method="POST" action="{{ route('customer_expense_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="customerModal_title">Edit</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 text-center">
                  
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Title</label>
                    <input type="text" class="form-control col-md-8" id="title" name="title" id="title" placeholder="Title">
                  </div>
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Price</label>
                    <input type="text" class="form-control col-md-8" id="price" name="price" placeholder="Price">
                  </div>
                  <div class="form-group">
                    <label class="col-md-3"style="clear: both;float: left;">Description</label>
                    <input type="text" class="form-control col-md-8" id="description" name="description" placeholder="Description">
                  </div>
                 
                  <!-- /.form-group -->
                </div>
              </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"> Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
$(function () {
    //Initialize Select2 Elements 
  $(".select2").select2({
    tags: true
  });

});
</script>


<script>
function edit_record(id)
{
  //alert(id);
    //$('#your_form').attr('action', 'http://uri-for-button1.com');
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/customer_expense_edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                     var title = result.title;                     
                     $("#title").val(title);

                     var price = result.price;                     
                     $("#price").val(price);
                     
                     var description = result.description;                     
                     $("#description").val(description);
                    //console.log(result.rgo);
                    
                  }});
             }

</script>
<script>
  $(function () {
    $("#table_body").DataTable();
  });
</script>
@endsection