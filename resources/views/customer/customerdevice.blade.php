@extends('layouts.master')
@extends('layouts.inputfield')
@section('content') 
<style>
  textarea.form-control {
    min-height: 35px;
    height: 37px;
    max-height: 38px;
  }
  .select2-container--default .select2-selection--single{
    border:transparent;
    height: auto;
    padding-bottom: 2px;
    border-bottom: 1px solid #ced4da;
  }
 
  .select2-container {
    display: inline-block;
  }
</style>


<section class="content">
 <h2 class="ml-3"><a href="{{url('/customer')}}" style="color: #343a40"><i class="fas fa-user" style="font-weight: normal;"></i></a> {{$customer_name->name}}</h2>
  <div class="container-fluid">
    
    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Device</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('customer_device_store')}}">
            @csrf
            <div class="row">
              <div class="col-md-6" style="display: none;">
                <div class="form-group">
                  <label class="col-md-3">Customer Name</label>
                  <input type="text" name="customer_id" class="form-control col-md-4" placeholder="Customer id" value="{{$data['customer_id']}}" hidden> 
                  <input type="text" name="customer_name" class="form-control col-md-4" placeholder="Customer Name" value="{{$customer_name->name}}" readonly>
                </div>
              </div>
                <!-- /.form-group -->
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Device Type</label> 
                  <select class="form-control col-md-4" name="device_type">
                    <option>System</option>
                    <option>Device</option>
                    <option>Route</option>
                    <option>other</option>
                  </select> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Router#</label>
                   <select class="form-control col-4 select2" name="serial_number" required>
                    <option value="">Select Router</option>
                    @foreach($data["devices"] as $devices) <!-- model name customer -->
                    <option value="{{ $devices->id }}" data-name="{{ $devices->serial_number }}">{{ $devices->serial_number }}</option>
                    @endforeach
                  </select>
                  
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">MAC</label>
                  <input type="text" class="form-control col-md-4" name="mac" placeholder="##-##-##-##-##-##" pattern="[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}" required title="MAC Format Required" required>
                </div>
              </div>
                <!-- /.form-group -->
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">IP</label>
                  <input type="text" class="form-control col-md-4" name="ip" placeholder="###.###.###.###"
                  pattern="\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"title="IP Format Required" required>
                </div>
              </div>
                <!-- /.form-group -->
              </div>
             
              <!-- /.col -->
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Vendor</label>
                  <input type="text" class="form-control col-md-4" name="company" placeholder="##" required>
                  <!-- <input type="text" class="form-control col-md-4" id="area_location" name="area_location" placeholder="Area Location" required> -->
                </div>
              </div>
                <!-- /.form-group -->
                <div class="col-md-6">
                <div class="form-group">
                 <label class="col-md-3"style="clear: both;float: left;">Package</label>
                  <select class="form-control select2" id="package" name="package" required>
                    <option value="">Select Package</option>
                    @foreach($data["packages"] as $packages)
                    <option value="{{ $packages->name }}" data-name="{{ $packages->name }}">{{ $packages->name }}</option>
                    @endforeach
                  </select> 
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Model</label>
                  <input type="text" class="form-control col-md-4" name="model" placeholder="##" required>
                </div>              
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Description</label>
                  <input type="text" class="form-control col-md-4" name="description" placeholder="##" required>
                </div>
              </div>
            </div>

            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Discount</label>
                  <input type="Number" class="form-control col-md-4" name="discount" placeholder="0" min="0" required>
                </div>
              </div>
            </div>
        
            <!-- /.row -->
            <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

          </div>
          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Device Details</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tabel_body" class="table table-bordered table-striped">
                <thead>
                <tr>                  
                  <th>Device Type</th>
                  <th>MAC</th>
                  <th>IP</th>
                  <th>Package</th>
                  <th>Action</th>                
                </tr>
                </thead>
                <tbody>
                @foreach($data["customer_devices"] as $customer_devices)
                <tr>                 
                  <td>{{$customer_devices->device_type}}</td>
                  <td>{{$customer_devices->mac}}</td>
                  <td>{{$customer_devices->ip}}</td>
                  <td>{{$customer_devices->package}}</td>                  
                  <td>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_record({{$customer_devices->id}})">Edit</button>                 
                  </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Device Type</th>
                  <th>MAC</th>
                  <th>IP</th>
                  <th>Package</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

    <div class="modal fade" id="EditModal-default">
      <form method="POST" action="{{ route('customer_device_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="customerModal_title">Edit Record</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                
                 <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Device Type</label>
                  <input type="text" class="form-control col-md-8" id="device_type" name="device_type" placeholder="Device Type">
                </div>               
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">MAC</label>
                  <input type="text" class="form-control col-md-8" id="mac" name="mac" placeholder="Mac">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">IP</label>
                  <input type="text" class="form-control col-md-8" id="ip" name="ip" placeholder="IP">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Package</label>
                  <input type="text" class="form-control col-md-8" id="package" name="package" placeholder="Balance">
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Vendor</label>
                  <input type="text" class="form-control col-md-8" id="company" name="company" placeholder="Company">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Model</label>
                  <input type="text" class="form-control col-md-8" id="model" name="model" placeholder="Package">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Description</label>
                  <input type="text" class="form-control col-md-8" id="description" name="description" id="description" placeholder="##">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Discount</label>
                  <input type="text" class="form-control col-md-8" id="discount" name="discount" id="discount_model" placeholder="0">
                </div>
               
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"> Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
$(function () {
    //Initialize Select2 Elements 
  $(".select2").select2({
    tags: true
  });

});
</script>


<script>
function edit_record(id)
{
  //alert(id);
    //$('#your_form').attr('action', 'http://uri-for-button1.com');
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/customer_device_edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var customer_name = result.customer_name;                     
                     $("#customer_name").val(customer_name);

                     var mac = result.mac;                     
                     $("#mac").val(mac);

                     var ip = result.ip;                     
                     $("#ip").val(ip);
                     
                     var device_type = result.device_type;                     
                     $("#device_type").val(device_type);

                     var package = result.package;                     
                     $("#package").val(package);

                      var company = result.company;                     
                     $("#company").val(company);

                      var model = result.model;                     
                     $("#model").val(model);

                     var description = result.description;                     
                     $("#description").val(description);

                     var discount_model = result.discount;                     
                     $("#discount").val(discount_model);

                    //console.log(result.rgo);
                    
                  }});
             }

</script>
<script>
  $(function () {
    $("#table_body").DataTable();
  });
</script>
@endsection