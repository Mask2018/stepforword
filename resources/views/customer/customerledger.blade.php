@extends('layouts.master')
@section('content')
<style>
</style>
<h2 class="ml-3"><a href="{{url('/customer')}}" style="color:#343a40"><i class="fas fa-user" style="font-weight: normal;"></i></a> {{$customer_name->name}}</h2>
<section class="content">
  <div class="container-fluid"> 
    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Legder</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="customerledger" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Invoice Number</th>
                    <th>Debit</th>
                    <th>Credit</th> 
                    <th>Balance</th>                 
                  </tr>
                </thead>
                <tbody>
                  @foreach($data["customer_ledgers"] as $customer_ledgers)
                  <tr>
                    <td>{{($customer_ledgers->updated_at)->format('d-m-Y')}}</a></td>
                    <td>{{$customer_ledgers->invoice_id}}</td>
                    <td>{{$customer_ledgers->debit}}</td>
                    <td>{{$customer_ledgers->credit}}</td>
                    <td>{{$customer_ledgers->balance}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

@endsection
@section('content_js')

@endsection