@extends('layouts.master')
@extends('layouts.inputfield')
@section('content') 
 <style>
  .select2-container--default .select2-selection--single{
    border:transparent;
    height: 37px;
    border-bottom: 1px solid #ced4da;
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow b{
    top: 65%;
  }
 
  .select2-container {
    display: inline-block;
  }
 </style>
<section class="content">
  <div class="container-fluid"> 
    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Customer</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('customer_store')}}">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3" style="clear: both;float: left;">Name</label>
                  <input type="text" name="name" class="form-control col-md-4" placeholder="Customer Name" required>
                </div>
              </div>
                <!-- /.form-group -->
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;"> CNIC</label>
                  <input type="text" class="form-control col-md-4" name="cnic" placeholder="#####-#######-#" pattern="\d{5}-\d{7}-\d{1}"title="CNIC Format Required" required>
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="row">
              <div class="col-md-6">
                <div class="form-group" id="dynamicCheck1">
                  <label class="col-md-3"style="clear: both;float: left;">Number</label>
                  <div class="phone-list col-4 float-left">
                    <div class="input-group phone-input">
                      <input type="number" name="number" class="form-control" placeholder="03########" required />
                    </div>
                  </div>
                  <button type="button" class="btn btn-success btn-sm btn-add-phone"><span class="glyphicon glyphicon-plus"></span>Add</button>
    
                  
                  <!-- <input type="text" class="form-control col-md-4" name="mac" placeholder="##-##-##-##-##-##" pattern="[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}" required title="MAC Format Required"> -->
                </div>
              </div>
                <!-- /.form-group -->
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Due Date</label>
                   <input type="number" class="form-control col-md-4" name="due_date" placeholder="1 - 30" title="range between 1 to 30 " min="1" max="30" required>
                 <!--  <input type="text" class="form-control col-md-4" name="ip" placeholder="###.###.###.###"
                  pattern="\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"title="IP Format Required" required> -->
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Balance</label>
                  <input type="number" class="form-control col-md-4" name="balance" placeholder="0" min="0" required>
                </div>
              </div>
                <!-- /.form-group -->
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Address</label>
                  <input type="text" class="form-control col-md-4" name="address" placeholder="Address" required>
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="row"> 
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Router #</label>
                   <select class="form-control select2" id="device_number" name="serial_number[]"  multiple="multiple" data-placeholder="Select Router" required>
                    <option value="">Select Router</option>
                    @foreach($data["devices"] as $devices)
                    <option value="{{ $devices->id }}" data-name="{{ $devices->serial_number }}">{{ $devices->serial_number }}</option>
                    @endforeach
                  </select>

                  <!-- <input type="text" class="form-control col-md-4" id="area_location" name="area_location" placeholder="Area Location" required> -->
                </div>
              </div>
                <!-- /.form-group -->
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Package</label>
                  <select class="form-control select2" id="package" name="package" required>
                    <option value="">Select Package</option>
                    @foreach($data["packages"] as $packages)
                    <option value="{{ $packages->name }}" data-name="{{ $packages->name }}">{{ $packages->name }}</option>
                    @endforeach
                  </select>
                  <!-- <select class="form-control col-md-4" name="package">
                    <option>Select</option>
                    <option>Package A</option>
                    <option>Package B</option>
                  </select> --> 
                </div>
              </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Description</label>
                  <textarea type="text" class="form-control col-md-4" name="desciption" placeholder="Description" required></textarea>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <div class="input-group">
                      <div class="custom-file col-6">
                        <input type="file" class="upload form-control" accept="image/*" name="file" multiple required />
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Front</span>
                      </div>
                  </div>
                  <div class="input-group">
                      <div class="custom-file col-6">
                        <input type="file" class="upload form-control" accept="image/*" name="file_second" multiple required />
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Back</span>
                      </div>
                  </div>
                </div> 
                <!-- /.form-group -->
              </div>

          </div>
            <!-- /.row -->

            <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

          </div>
          <!-- /.card-body -->
    </div>

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Customers</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="customer" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Address</th>
                  <th>PackageName</th>
                  <th>Balance</th>
                  <th>Add Device</th>
                  <th>Add Expense</th>
                  <th>Action</th>                
                </tr>
                </thead>
                <tbody>
                @foreach($data["customers"] as $customers)
                <tr>
                  <td><a href="{{route('customer_ledger',$customers->id)}}"> {{$customers->name}}</a></td>
                  <td>{{$customers->address}}</td>
                  <td>{{$customers->package}}</td>
                  <td>{{$customers->balance}}</td>
                  <td>
                   <button type="button" class="btn btn-default"><a href="{{route('customer_device',$customers->id)}}">Add</a></button>
                  </td>
                  <td>
                   <button type="button" class="btn btn-default"><a href="{{route('customer_expense',$customers->id)}}">Add</a></button>
                  </td>                  
                  <td>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal-default" onclick="edit_customer({{$customers->id}})">Edit</button>
                  </td>

                  
                    <!-- <button type="button" class="btn btn-default" onclick="customerdevice({{$customers->id}})">Add
                  </button> -->
                   <!-- <a href="customer/{{$customers->id}}/edit" class="btn btn-primary">Edit</a>
                    <a href="CustomerController@edit/{{$customers->id}}" class="btn btn-primary">Edit</a> -->
                   
                  
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Address</th>
                  <th>PackageName</th>
                  <th>Balance</th>
                  <th>Add Device</th>
                  <th>Add Expense</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
    </div>
  </div>
</section>

    <div class="modal fade" id="EditModal-default">
      <form method="POST" action="{{ route('customer_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="customerModal_title">Edit Customer</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Name</label>
                  <input type="text" class="form-control col-md-8" id="name" name="name" placeholder="name">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">CNIC</label>
                  <input type="text" class="form-control col-md-8" id="cnic" name="cnic" placeholder="cnic">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Number</label>
                  <input type="text" class="form-control col-md-8" id="number" name="number" placeholder="Number">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Due Date</label>
                  <input type="text" class="form-control col-md-8" id="due_date" name="due_date" placeholder="Due Date">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Balance</label>
                  <input type="text" class="form-control col-md-8" id="balance" name="balance" placeholder="Balance">
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Address</label>
                  <input type="text" class="form-control col-md-8" id="address" name="address" placeholder="Address">
                </div>
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Package</label>
                  <input type="text" class="form-control col-md-8" id="package" name="package" placeholder="Package">
                </div>

                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Description</label>
                  <input type="text" class="form-control col-md-8" id="desciption" name="desciption" placeholder="desciption">
                </div>                
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"> Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

   /* $('#previous_dp').on('change', function (e) {
    }*/

  }); 
</script>

<script type="text/JavaScript">
      $(function(){
        
        $(document.body).on('click', '.btn-remove-phone' ,function(){
          $(this).closest('.phone-input').remove();
        });
        $('.btn-add-phone').click(function(){
          var index = $('.phone-input').length + 1;
          $('.phone-list').append(''+
              '<div class="input-group phone-input">'+
                '<input type="number" name="number" class="form-control" placeholder="03##########" />'+                
                '<span class="input-group-btn">'+
                  '<button class="btn btn-danger btn-remove-phone ml-2" type="button"><span class="glyphicon glyphicon-remove"></span>-</button>'+
                '</span>'+
              '</div>'
          );
        });
      
      });
</script>
<!-- <script>
   function customerdevice(id)
    {
      window.location.href='customer/customerdevice.php';
    }
  
</script> -->
<script>
function edit_customer(id)
{
  //alert(id);
    //$('#your_form').attr('action', 'http://uri-for-button1.com');
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/customer/edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var name = result.name;                     
                     $("#name").val(name);

                     var address = result.address;                     
                     $("#address").val(address);

                     var package = result.package;                     
                     $("#package").val(package);

                     var cnic = result.cnic;                     
                     $("#cnic").val(cnic);

                     var balance = result.balance;                     
                     $("#balance").val(balance);

                     var number = result.number;                     
                     $("#number").val(number);

                     var due_date = result.due_date;                     
                     $("#due_date").val(due_date);
                     
                     var address = result.address;                     
                     $("#address").val(address);

                     var desciption = result.desciption;                     
                     $("#desciption").val(desciption);
                    //console.log(result.rgo);
                    // console.log(regional_offices);
                     /*$('#adduserpopup .modal-header').children('h4').text('Update User');
                    $('#adduserpopup').modal('show');*/
                  }});

 


  $("#ledger").click(function Order()//Function to Select Product And Quantity
    { 
      var customer_id = $("#customer").val();
          
          var invoice_detail_data = [];
          var invoice_data = []; 
          
          // $(this).attr("disabled", true);
          var inv = {

            'customer_id' : customer_id,
            'amount'      : $("#TotalAmount1").val(),
            'paid'        : 0,
            'discount' : 1,
            'balance'     : $("#TotalAmount1").val(),
            'total'       : $("#TotalAmount1").val(),
          };
              invoice_data.push(inv);

             $('#order_table tr').each(function(row,tr){

              if($(tr).find('td:eq(0)').text() == "")
              {

              }
              else
              {
                  var details = 
                  {
                   'product_id' : $(tr).find('td:eq(0)').text(),
                   'order_quantity' : $(tr).find('td:eq(2)').text(),
                   'unit_rate' : $(tr).find('td:eq(3)').text(),
                   'product_rate' : $(tr).find('td:eq(4)').text(),
                  };
                  invoice_detail_data.push(details);
              }
          });
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/invoice/store') }}",
                  method: 'POST',
                  data: {'invoice_data':invoice_data, 'invoice_detail_data':invoice_detail_data},
                  success: function(result){
                     window.location = '/invoice/view/' + result;
                    }});
          });

} 
</script>
<script>
  $(function () {
    $("#customer").DataTable();
  });
</script>
@endsection