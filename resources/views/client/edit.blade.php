@extends('layouts.master')
@extends('layouts.inputfield')
@section('content')
<style>
  .select2-container{ 
      display: inline-block;
      min-width: 41.66%;
    }

  .select2-container--default .select2-selection--single{
      height: auto;
      padding: 3px;
      border: transparent;
      border-bottom: 1px solid #ced4da;
    }
</style>

<section class="content">
  <div class="container-fluid">
    <div class="card collapsed-card">
          <div class="card-header">
            <h3 class="card-title">Add Request</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form method="POST" action="{{route('clientrequest_store')}}">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3" style="clear: both;float: left;">Name</label>
                  <input type="text" name="name" class="form-control col-md-4" placeholder="Client Name" required>
                </div>
              </div>
                <!-- /.form-group --> 
              <div class="col-md-6">
                <div class="form-group">
                   <label class="col-md-3"style="clear: both;float: left;">Number</label>
                   <input type="number" name="number" class="form-control col-md-4" placeholder="Client Number" required>
                </div>
              </div>
                <!-- /.form-group -->
            </div>
              <!-- /.col -->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Area</label>
                  <input type="text" class="form-control col-md-4" name="area_location" placeholder="area" required>
                </div>
              </div>
                <!-- /.form-group -->
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3"style="clear: both;float: left;">Description</label>
                  <input type="text" class="form-control col-md-4" name="description" placeholder="description" required>
                </div>
              </div>
                <!-- /.form-group -->
            </div>
              <!-- /.col -->

            <!-- /.row -->
            <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

          </div>
          <!-- /.card-body -->
    </div>

<div class="card">
            <div class="card-header">
              <h3 class="card-title">All Request</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="product" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Number</th>
                  <th>Area</th>
                  <th>Description</th>
                  <th>Action</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($data["clientrequests"] as $clientrequests)

                <tr>
                  <td>{{$clientrequests->name}}</td>
                  <td>{{$clientrequests->number}}</td>
                  <td>{{$clientrequests->area_location}}</td>
                  <td>{{$clientrequests->description}}</td>                  
                  <td>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#EditModal" onclick="edit_product({{$clientrequests->id}})"> Edit
                    </button>                   
                  </td>
                </tr>

                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Number</th>
                  <th>Area</th>
                  <th>Description</th>
                  <th>Action</th>                  
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div> 
</div>
</section>

    <div class="modal fade" id="EditModal">
      <form method="POST" action="{{ route('clientrequest_update')}}">
        @csrf
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="productModal_title">Edit Request</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Name</label>
                  <input type="text" class="form-control col-7" id="name" name="name" placeholder="name">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Number</label>
                  <input type="number" class="form-control col-7" id="number" name="number" placeholder="number">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Area</label>
                  <input type="text" class="form-control col-7" id="area_location" name="area_location" placeholder="area">
                </div>
                <div class="form-group">
                  <label class="col-4"style="clear: both;float: left;">Description</label>
                  <input type="text" class="form-control col-7" id="description" name="description" placeholder="description">
                </div>
                
                              
                <!-- /.form-group -->
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"> Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('content_js')

<script>
function edit_product(id) //db view only
{
  //alert(id);
    //$('#your_form').attr('action', 'http://uri-for-button1.com');
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/clientrequest/edit') }}",
                  method: 'POST',
                  data: {
                     id: id,
                  },
                  success: function(result){
                     console.log(result);
                    /* alert(result.cnic);*/
                    
                    var name = result.name;                     
                     $("#name").val(name);

                     var number = result.number;                     
                     $("#number").val(number);

                     var area_location = result.area_location;                     
                     $("#area_location").val(area_location);

                     var description = result.description;                     
                     $("#description").val(description);
                  }});
}
</script>
<script>
  $(function () {
    $("#product").DataTable();
  });
</script>
@endsection