<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Product extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('products', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            //$table->primary('id');
            $table->integer('vendor_id')->nullable();

            $table->string('name')->unique();
            $table->string('category');

            $table->integer('sale_price');
            $table->integer('purchase_price');

            $table->integer('quantity');
            $table->integer('sold');

            $table->integer('stock');
            
           // $table->rememberToken();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
