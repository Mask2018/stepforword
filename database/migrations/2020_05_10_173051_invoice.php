<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('invoices', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            /*$table->primary('id');*/
            $table->integer('customer_id');
        
            $table->integer('sub_amount');
            $table->integer('balance');

            $table->integer('paid');
            $table->integer('discount')->nullable();

            $table->integer('total');
                        
           // $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
