<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShareHolderLedger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('share_holder_ledgers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('shareholder_id');
            $table->string('invoice_id');
            $table->string('invoice_type');
            $table->text('desciption');
            $table->integer('debit');
            $table->integer('credit'); 
            $table->integer('balance');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
