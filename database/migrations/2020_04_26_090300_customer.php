<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('customers', function (Blueprint $table) {
            
            $table->bigIncrements('id');
          //  $table->primary('id');
            
            $table->string('name');
            $table->string('cnic')->unique();

            $table->integer('number');
            $table->integer('due_date');

            $table->integer('balance');
            $table->string('address');
            
            $table->string('serial_number');
            $table->string('package');

            $table->text('desciption');
            $table->text('file');
            $table->text('file_second');
            
           // $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
