<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Package extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('packages', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            //$table->primary('id');

            $table->string('name');
            $table->integer('upload_speed');

            $table->text('upload_unit');
            $table->text('download_unit');

            $table->integer('download_speed');
            $table->integer('rate');

            $table->string('vendor');
            
           // $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
