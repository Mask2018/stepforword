<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaidReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('paid_receipts', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->integer('vendor_id');

            $table->integer('paid_amount');            
            $table->string('being')->nullable();

            $table->string('r_type')->nullable();
            $table->string('bank_name');            
            
           // $table->rememberToken();
            $table->timestamps();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
