<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceivedReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('received_receipts', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->integer('customer_id');

            $table->integer('received_amount');            
            $table->string('being')->nullable();

            $table->string('r_type')->nullable();
            $table->string('bank_name');            
            
           // $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
