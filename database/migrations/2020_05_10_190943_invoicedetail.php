<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invoicedetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
         Schema::create('invoice_details', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->integer('invoice_id');
            $table->integer('product_id');

            $table->integer('order_quantity');

            $table->integer('unit_rate');
            $table->integer('product_rate');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
