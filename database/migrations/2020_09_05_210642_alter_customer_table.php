<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('number')->nullable()->change();
            $table->integer('due_date')->nullable()->change();

            $table->integer('balance')->nullable()->change();
            $table->string('address')->nullable()->change();
            
            $table->string('serial_number')->nullable()->change();
            $table->string('package')->nullable()->change();

            $table->text('desciption')->nullable()->change();
            $table->text('file')->nullable()->change();
            $table->text('file_second')->nullable()->change();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {

        });
    }
}
