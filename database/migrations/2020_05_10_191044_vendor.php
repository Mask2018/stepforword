<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    { 
         Schema::create('vendors', function (Blueprint $table) {
           
            $table->bigIncrements('id');
           // $table->primary('id');
            $table->string('name')->unique();
            $table->string('cnic')->unique();

            $table->integer('sale');
            $table->integer('purchase');

            $table->integer('credit');
            $table->integer('balance');

            $table->string('address');
            $table->string('package');

            $table->integer('expenses');  
            $table->string('area_location');

            $table->text('desciption');
            $table->text('file')->nullable();
            $table->text('file_second')->nullable();
            
            
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
