<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dpmanagment extends Migration
{
    /** 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('dpmanagments', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            //$table->primary('id');

            $table->string('dp_number');
            $table->string('location');

            $table->string('description');
            $table->string('previous_dp');

            $table->string('next_dp');
            $table->string('device_serial');

            $table->string('address');
            $table->string('created_by');
            
           // $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
