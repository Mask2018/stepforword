<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerDevice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('customer_devices', function (Blueprint $table) {
            
            $table->bigIncrements('id');
          //  $table->primary('id');

            $table->integer('customer_id');
            $table->string('customer_name');

            $table->string('mac')->unique();
            $table->string('ip')->unique();

            
            $table->string('device_type');
            $table->string('package');

            $table->string('company');
            $table->string('model');

            $table->string('description');
            $table->integer('discount');
            
           // $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
