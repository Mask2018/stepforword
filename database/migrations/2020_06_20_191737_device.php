<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Device extends Migration
{ 
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('devices', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            //$table->primary('id');

            $table->string('serial_number');
            $table->string('ports');

            $table->string('company_name');
            $table->string('description');

            $table->string('model');
            $table->string('price');

            $table->string('stock_status');
            $table->string('created_by');

            $table->string('allocated_by');
            
           // $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
