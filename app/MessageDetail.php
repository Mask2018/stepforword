<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageDetail extends Model
{
     protected $fillable = ['message_id','message'];
}
