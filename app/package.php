<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    protected $fillable = ['name','upload_speed','upload_unit','download_speed','download_unit','rate','vendor'];
}
