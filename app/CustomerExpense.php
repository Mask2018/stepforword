<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerExpense extends Model
{
    //
    protected $fillable = [
      'customer_id','title', 'price', 'description'
    ];
    public function customer()
    {
    	return $this->belongsTo('App\customer','customer_id');
    }
}
