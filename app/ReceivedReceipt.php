<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceivedReceipt extends Model
{
    //
    protected $fillable = ['customer_id','received_amount','r_type','bank_name','being'];

    public function customer()
    {
    	return $this->belongsTo('App\customer','customer_id');
    }
}
