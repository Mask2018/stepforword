<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareHolderLedger extends Model
{
    protected $fillable = [
       'shareholder_id', 'invoice_id', 'invoice_type', 'desciption', 'debit', 'credit', 'balance'
    ];
}