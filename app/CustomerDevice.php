<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDevice extends Model
{
    //
    protected $fillable = [
      'customer_id', 'customer_name', 'mac', 'ip', 'device_type', 'package', 'company','model','description','discount'
    ];

   /* public function customers()
    {
    	dd($this->belongsTo('App\customer', 'customer_id'));
    }*/
}
