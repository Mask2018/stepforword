<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchase extends Model
{
    //
    protected $fillable = [
      'id','vendor_id', 'amount', 'balance','paid','discount','total'
    ];
}
