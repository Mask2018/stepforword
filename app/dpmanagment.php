<?php
 
namespace App;

use Illuminate\Database\Eloquent\Model;

class dpmanagment extends Model
{
    //
    protected $fillable = [
     'dp_number', 'location', 'description', 'created_by','address','previous_dp','next_dp','device_serial'
    ];
}
