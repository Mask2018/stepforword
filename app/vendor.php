<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vendor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'cnic','sale','purchase','credit','balance', 'address','package','expenses','area_location', 'desciption','file','file_second'
    ];
}
