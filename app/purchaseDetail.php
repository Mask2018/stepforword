<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchaseDetail extends Model
{
    //
     protected $fillable = [
    'id','purchase_id','product_id','invoice_type','order_quantity','unit_rate','product_rate'
    ]; 
}
