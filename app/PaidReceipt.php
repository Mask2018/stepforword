<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class PaidReceipt extends Model
{
    protected $fillable = ['vendor_id','paid_amount', 'r_type','bank_name','being'];

    public function vendor()
    {
    	return $this->belongsTo('App\vendor','vendor_id');
    }
}