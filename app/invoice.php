<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
    //
    protected $fillable = [
      'id','customer_id', 'sub_amount', 'balance','paid','discount','total'
    ];
} 