<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorLedger extends Model
{
    protected $fillable = [
       'vendor_id', 'invoice_id', 'invoice_type', 'desciption', 'debit', 'credit', 'balance'
    ];
}
