<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    //
    protected $fillable = [
     'serial_number', 'company_name', 'stock_status', 'created_by','allocated_by','description','model','price','ports'
    ];
}
