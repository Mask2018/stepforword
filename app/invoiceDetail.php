<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoiceDetail extends Model
{
     protected $fillable = [
    'id','invoice_id','product_id','order_quantity','unit_rate','product_rate'
    ]; 
}
