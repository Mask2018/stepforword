<?php
 
namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareHolder extends Model
{
     protected $fillable = [
       'name', 'cnic','sale','purchase','credit','balance','assign', 'address','package','expenses','area_location', 'desciption','file','file_second'
    ]; 
}
