<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerLedger extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'customer_id', 'invoice_id', 'invoice_type', 'desciption', 'debit', 'credit', 'balance'
    ];
    public function customer()
    {
    	return $this->belongsTo('App\customer','customer_id');
    }
}
