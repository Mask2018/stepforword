<?php

namespace App\Http\Controllers;
use App\ClientRequest;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array();
        $data["title"]='Client Request';
        $data["clientrequests"]=ClientRequest::all();
        return view ('client.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $clientrequest = $request->all();
        $clientrequest = ClientRequest::create($clientrequest);
        return redirect('/clientrequest')->with('flash_success', 'Client request Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $client)
    {
        //
        $id = $client["id"];
        $Obj=ClientRequest::find($id);
        return response($Obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        DB::table('client_request') /*db table*/
        ->where('id',$request["id"])
        ->update(['name'       => $request["name"], 
                'cnic'         => $request["cnic"],
                'sale'         => $request["sale"],
                'purchase'     => $request["purchase"],
                'credit'       => $request["credit"],
                'balance'      => $request["balance"],
                'assign'      => $request["assign"], 
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
