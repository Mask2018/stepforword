<?php

namespace App\Http\Controllers;

use App\invoiceDetail;
use App\invoice;
use App\product;
use App\customer;
use Illuminate\Http\Request;

class InvoiceDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array();
       // $data["title"] = "Invoice Detail";
        $data["product"] = product::all(); //model::all
        $data["customer"] = customer::all();

        //$data["invoiceDetail"] = invoiceDetail::all();
        return view('invoiceDetail.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $invoiceDetail = $request->all();
         // dd($customer);
        //variable = modelname::funation($array);
        $invoiceDetail = invoiceDetail::create($invoiceDetail);
    return redirect('/invoiceDetail')->with('flash_success', 'invoice Saved Successfully');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\invoiceDetail  $invoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function show(invoiceDetail $invoiceDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\invoiceDetail  $invoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(invoiceDetail $invoiceDetail)
    {
        //
        $id = $invoice["id"];
        $Obj=invoice::find($id);        
        return response($Obj);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\invoiceDetail  $invoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        DB::table('invoices') /*db table*/
        ->where('id',$request["id"])
        ->update(['name' => $request["name"], 
                'category' => $request["category"],
                'salePrice' => $request["salePrice"],
                'purchasePrice' => $request["purchasePrice"],
                'quantity' => $request["quantity"],
                'sold' => $request["sold"],
                'stock' => $request["stock"],                
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\invoiceDetail  $invoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(invoiceDetail $invoiceDetail)
    {
        //
         $data = array();
        $data["title"] = "Blocked Invoices";
        return view('invoice.blocked', compact('data'));
    }
}
