<?php
 
namespace App\Http\Controllers;
use App\ShareHolder;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\ShareholderLedger;
use Storage;
use DB;

class ShareHolderController extends Controller
{
       public function index()
    {
        $data = array();
        $data["title"] = "Share Holder";
        $data["shareholders"] = ShareHolder::all();
        return view('shareholder.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {/*
        $this->validate($request,[
    'name'=> 'required|unique:customers,name|regex:/^[a-zA-Z0-9\s\-]*$/|max:50',
            'cnic'=> 'required|regex:/^([0-9\-]*)$/|min:10|max:16', 
                    //allow ony number and dash
            'sale'=> 'required|integer',
            'purchase'=>'required|integer',
            'credit'=> 'required|integer',
            'balance'=> 'required|integer',
            'assign'=> 'required',
            'address'=> 'required',
            'package'=> 'required',
            'expenses'=> 'required|integer',
            'area_location'=> 'required',
            'desciption'=> 'required|string',
        ]);*/
        $shareholder = $request->all();
        Storage::disk('local')->put($request->file, 'Contents');
        //variable = modelname::funation($array);
        $shareholder_id = ShareHolder::create($shareholder)->id;
        /*Vendor Ledger Portion*/
        
        $share_holder_ledger = array("shareholder_id"=>$shareholder_id, "invoice_id"=>$shareholder_id, "invoice_type"=> "OB", "desciption"=>"Opening Balance", "debit"=>0, "credit"=>0, "balance"=>$request->balance);
        $share_holder_ledger = ShareHolderLedger::create($share_holder_ledger);
        
        /*Vendor Ledger Portion End*/
        return redirect('/shareholder')->with('flash_success', 'Share Holder Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $shareholder)
    {
        $id = $shareholder["id"];
        $Obj=ShareHolder::find($id);
        return response($Obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

         DB::table('share_holders') /*db table*/
        ->where('id',$request["id"])
        ->update(['name'       => $request["name"], 
                'cnic'         => $request["cnic"],
                'sale'         => $request["sale"],
                'purchase'     => $request["purchase"],
                'credit'       => $request["credit"],
                'balance'      => $request["balance"],
                'assign'      => $request["assign"],
                'address'      => $request["address"],
                'package'      => $request["package"],
                'expenses'     => $request["expenses"],
                'area_location' => $request["area_location"],
                'desciption'   => $request["desciption"],   
            ]);
      //  return redirect('/shareholder')->with('flash_success', 'ShareHolder Saved Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function blocked()
    {
        $data = array();
        $data["title"] = "Blocked ShareHolder";
        return view('shareholder.blocked', compact('data'));
    }


    public function shareholder_ledger($id)
    { 
        $data = array();
        $data["title"] = "ShareHolder Ledger";
        $data["share_holder_ledgers"] = ShareholderLedger::where("shareholder_id",$id)->get();
       // dd($id);
        //dd($data["customer_ledgers"]);
        return view('shareholder.shareholderledger',compact('data'));
    }
}
