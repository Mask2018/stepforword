<?php
 
namespace App\Http\Controllers;
use App\dpmanagment;
use App\Device;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Storage;
use DB;
 
class DpmanagmentController extends Controller
{
    //
	public function index()
    {
        $data = array();
        $data["title"] = "DP";
        $data["devices"] = Device::all();
        $data["dpmanagments"] = dpmanagment::all();
        return view('dpmanagment.index', compact('data')); 
        /*folder name then file name*/
    }
 
    public function store(Request $request)
    { 
        /* $this->validate($request,[
       
            'ip'=>    'required|regex:/^[0-9\.]*$/|min:8|max:15',
            'created_by'=> 'required',
        ]);*/
         
        $dpmanagment = $request->all();
 
        $new_dps = $request->input('next_dp'); 
        $new_dp_string = implode(",",$new_dps);
            
            $dpmanagment['next_dp'] = $new_dp_string;
             //dd($dpmanagment);
            $dpmanagment = dpmanagment::create($dpmanagment); 
        return redirect('/dp')->with('flash_success', 'DP Saved Successfully');
    }

    public function edit(Request $dpmanagment)
    { 
        $id = $dpmanagment["id"];
        $Obj=dpmanagment::find($id);
        return response($Obj);
    }

    public function update(Request $request)
    {

         DB::table('dpmanagments') /*db table*/
        ->where('id',$request["id"])
        ->update(['dp_number' => $request["dp_number"], 
                'location' => $request["location"],
                'created_by' => $request["created_by"],
                'description' => $request["description"],
                'address' => $request["address"],                
                'previous_dp' => $request["previous_dp"],
                'next_dp' => $request["next_dp"],
                'device_serial' => $request["device_serial"],
            ]);
       /* return redirect('/customer')->with('flash_success', 'Customer Saved Successfully');
   */ }

    public function blocked()
    {
        $data = array();
        $data["title"] = "Blocked DP";
        return view('dpmanagment.blocked', compact('data'));
    }


}
