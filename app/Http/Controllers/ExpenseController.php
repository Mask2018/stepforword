<?php

namespace App\Http\Controllers;
use App\expense;
use Illuminate\Http\Request;


class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["title"] = "Expense";
        $data["expenses"] = expense::all();
        return view('expense.index', compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $this->validate($request,[
            'expense'=> 'required',
            'description'=> 'required',
            'price'=> 'required',
        ]);
        $expense = $request->all();
        //variable = modelname::funation($array);
        $expense = expense::create($expense);
        return redirect('/expense')->with('flash_success', 'expense Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $expense)
    {
        //
        $id = $expense["id"];
        $Obj=expense::find($id);   
        return response($Obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
         DB::table('expenses') /*db table*/
        ->where('id',$request["id"])
        ->update(['expense' => $request["expense"], 
                'description' => $request["description"],
                'price' => $request["price"],
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
