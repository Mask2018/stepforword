<?php 
namespace App\Http\Controllers;
use App\Complain;
use App\customer;
use Illuminate\Http\Request;

class ComplainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["title"] = "Complain";
        $data["complains"] = Complain::all();
        $data["customers"] = customer::all(); 
        return view('complain.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $complain = $request->all();
        $complain_id = Complain::create($complain)->id;
        return redirect('/complain')->with('flash_success', 'Complain Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $complain)
    {
        //
        $id = $complain["id"];
        $data["complain"]= Complain::find($id);
        $data["customer"]= customer::find($data["complain"]->customer_id);
        //dd($data);
        return response($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        DB::table('complains') /*db table*/
        ->where('id',$request["id"])
        ->update(['name' => $request["name"], 
                'title' => $request["title"],
                'desciption' => $request["desciption"]             
            ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
