<?php

namespace App\Http\Controllers;
use App\ReceivedReceipt;
use App\customer;
use Illuminate\Http\Request;

class ReceivedReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array();
        $data["title"] = "Receiving Receipt";
        $data["received_receipts"] = ReceivedReceipt::all();
        $data["customers"] = customer::all();
        return view('receipt.received_receipt', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ReceivedReceipt = $request->all();
        //variable = modelname::funation($array);
        $ReceivedReceipt = ReceivedReceipt::create($ReceivedReceipt);
        return redirect('/received_receipt')->with('flash_success', 'Receipt Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $ReceivedReceipt = $request->all();
        //variable = modelname::funation($array);
        $ReceivedReceipt = ReceivedReceipt::create($ReceivedReceipt);
        return redirect('/receipt.received_receipt')->with('flash_success', 'Receipt Saved Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $receivedreceipt)
    {
        //
        $id = $receivedreceipt["id"];
        $Obj=ReceivedReceipt::find($id);
        return response($Obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
