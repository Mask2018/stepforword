<?php

namespace App\Http\Controllers;
use App\Device; //model name
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Storage;
use DB;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $data = array();
        $data["title"] = "Router";
        $data["devices"] = Device::all(); //db nd model name
        return view('device.index', compact('data')); 
        /*folder name then file name*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'ports'=> 'required',
        ]);

       $device = $request->all();
        //variable = modelname::funation($array);
        $device = Device::create($device);
        return redirect('/device')->with('flash_success', 'Device Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $device)
    {
        $id = $device["id"];
        $Obj=Device::find($id);
        return response($Obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        DB::table('devices') /*db table*/
        ->where('id',$request["id"])
        ->update(['serial_number' => $request["serial_number"], 
                'company_name' => $request["company_name"],
                'model' => $request["model"], 
                'price' => $request["price"],
                'ports' => $request["ports"],
                'description' => $request["description"],
                'stock_status' => $request["stock_status"],
                'created_by' => $request["created_by"],
                'allocated_by' => $request["allocated_by"],               
            ]);
        // return redirect('/device')->with('flash_success', 'Customer Saved Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function blocked()
    {
        $data = array();
        $data["title"] = "Blocked Device";
        return view('device.blocked', compact('data'));
    }

}
