<?php

namespace App\Http\Controllers;

use App\vendor;
use App\product;
use App\purchase;
use App\purchaseDetail;
use App\VendorLedger;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array();
        $data["title"] = "Purchase Invoice";
       // $data["purchase"] = purchase::all();
        $data["product"] = product::all(); //model::all
        $data["vendor"] = vendor::all();
        return view('invoice.purchaseinvoice', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         /*Saving Invoice*/
        $purchase_data = $request["purchase_data"][0];
        $purchase_id = purchase::create($purchase_data)->id;

        /*Saving Invoice Detail*/
        foreach ($request["purchase_detail_data"] as $purchase_detail_data)
        {
            $purchase_detail_data["purchase_id"] = $purchase_id;
            $purchase_detail = purchaseDetail::create($purchase_detail_data); 
                                //model name
        }

        /*Vendor Ledger Portion*/
        
        $vendor_ledger = array(
            "vendor_id"=>$request["purchase_data"][0]["vendor_id"],
            "invoice_id"=>$purchase_id,
            "invoice_type"=> "Sale Invoice",
            "desciption"=>"None",
            "debit"=>$request["purchase_data"][0]["total"],
            "credit"=>$request["purchase_data"][0]["paid"],
            "balance"=>0);
        $save_vendor_ledger = VendorLedger::create($vendor_ledger); 
        /*Customer Ledger Portion End*/
        //dd($invoice_detail);
    return json_encode($purchase_id);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(purchase $purchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(purchase $purchase)
    {
        //
        $id = $purchase["id"];
        $Obj=purchase::find($id);  //model name      
        return response($Obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
         DB::table('purchases') /*db table*/
        ->where('id',$request["id"])
        ->update(['name' => $request["name"], 
                'category' => $request["category"],
                'sale_price' => $request["sale_price"],
                'purchase_price' => $request["purchase_price"],
                'quantity' => $request["quantity"],
                'sold' => $request["sold"],
              ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(purchase $purchase)
    {
        //
        $data = array();
        $data["title"] = "Blocked PurchaseInvoices";
        return view('purchase.blocked', compact('data'));
    }


    public function InvoiceView($purchase_id)
    {
        $index = 0;
        $data = array();
        $data["title"] = "PurchaseInvoice";
        $data["purchase"] = purchase::find($purchase_id);

        $data["purchase_detail"] = purchaseDetail::where("purchase_id",$purchase_id)->get();

        $data["vendor"] = vendor::find($data["purchase"]["vendor_id"]);
        foreach ($data["purchase_detail"] as $purchase) {
            $product = product::find($purchase["product_id"]);

            $data["purchase_detail"][$index]["product_name"] = $product["name"];
            $index++;
        }
        //dd($data);
        return view('invoice.purchaseview',compact('data')); // invoice is a folder name
   
    }
}
