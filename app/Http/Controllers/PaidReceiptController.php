<?php

namespace App\Http\Controllers;
use App\PaidReceipt;
use App\vendor;
use Illuminate\Http\Request;

class PaidReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array();
        $data["title"] = "Payment Receipt";
        $data["paid_receipts"] = PaidReceipt::all();
        $data["vendors"] = vendor::all();
        return view('receipt.paid_receipt', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //
        $PaidReceipt = $request->all();
        //variable = modelname::funation($array);
        $PaidReceipt = PaidReceipt::create($PaidReceipt);
        return redirect('/paid_receipt')->with('flash_success', 'Receipt Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $paidreceipt)
    {
        //
        $id = $paidreceipt["id"];
    
        $Obj=PaidReceipt::find($id);
        return response($Obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
