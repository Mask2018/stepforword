<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Storage;
use DB;
 
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["title"] = "Product";
        $data["products"] = product::all();
        return view('product.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
         $this->validate($request,[
            'sale_price'=> 'required|integer|min:0',
            'purchase_price'=> 'required|integer|min:0',
            'quantity'=> 'required|integer|min:0',
            'sold'=> 'required|integer|min:0',
            'stock'=> 'required|integer|min:0',
            'name'=> 'required',
            'category'=> 'required',
        ]);


        $product = $request->all(); 
        //dd($product);
        //variable = modelname::funation($array);
        $product = product::create($product);
        return redirect('/product')->with('flash_success', 'product Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $product)
    {
    
        $id = $product["id"];
        $Obj=product::find($id);
        
        return response($Obj);

        //dd($custObj);
        /*return $id;*/
        //return view('customer.edit')->with('custObj',$custObj);
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

         DB::table('products') /*db table*/
        ->where('id',$request["id"])
        ->update(['name' => $request["name"], 
                'category' => $request["category"],
                'sale_price' => $request["sale_price"],
                'purchase_price' => $request["purchase_price"],
                'quantity' => $request["quantity"],
                'sold' => $request["sold"],
                'stock' => $request["stock"],                
            ]);
       /* return redirect('/customer')->with('flash_success', 'Customer Saved Successfully');
   */ }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(product $product)
    {
        //
    }

    public function blocked()
    {
        $data = array();
        $data["title"] = "Blocked Products";
        return view('product.blocked', compact('data'));
    }
}
