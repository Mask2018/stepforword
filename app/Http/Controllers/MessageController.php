<?php

namespace App\Http\Controllers;
use App\Message;
use Illuminate\Http\Request;
use App\MessageDetail;
use Storage;
use DB; 

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array();
        $data["title"] = "Message";
        $data["messages"] = Message::all(); //db nd model name
        return view('message.index', compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

    public function store(Request $request)
    {
        //
         $this->validate($request,[
            'name'=> 'required', 
            'phone_number'=> 'required|regex:/^[0-9\-]*$/|min:1|max:15',
        ]);

       $message = $request->all();
        //variable = modelname::funation($array);
        $message = Message::create($message);
        return redirect('/message')->with('flash_success', 'Device Saved Successfully');
    }



     public function send(Request $request)
    {
        $this->validate($request,[ 'message'=> 'required', ]);
       $message = $request->all();
        //variable = modelname::funation($array);
        $message_details = MessageDetail::create($message);
        return redirect('/message')->with('flash_success', 'Device Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $message)
    {
        //
        $id = $message["id"];
        $Obj=Message::find($id);
        return response($Obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        DB::table('messages') /*db table*/
        ->where('id',$request["id"])
        ->update(['name' => $request["name"], 
                'phone_number' => $request["phone_number"],
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function destroy($id)
    {
        //
    }


    public function blocked()
    {
        $data = array();
        $data["title"] = "Blocked Number";
       // return view('message.blocked', compact('data'));
    }

    public function message_detail($id)
    { 
        $data = array();
        $data["title"] = "Message Detail";
        $data["message_details"] = MessageDetail::where("message_id",$id)->get();
       // dd($id);
        //dd($data["message_details"]);
        return view('message.messagedetail',compact('data'));
    }
}
