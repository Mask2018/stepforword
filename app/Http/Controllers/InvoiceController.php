<?php

namespace App\Http\Controllers;

use App\invoice;
use App\invoiceDetail;
use App\product;
use App\customer;
use App\CustomerLedger;
use Illuminate\Http\Request;
 
class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        $data = array();
        $data["title"] = "Invoice";
        //$data["invoices"] = invoice::all();
        $data["product"] = product::all(); //model::all
        $data["customer"] = customer::all();
        return view('invoice.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        /*Saving Invoice*/
        $invoice_data = $request["invoice_data"][0];
        $invoice_id = invoice::create($invoice_data)->id;

        /*Saving Invoice Detail*/
        foreach ($request["invoice_detail_data"] as $invoice_detail_data) {
        $invoice_detail_data["invoice_id"] = $invoice_id;
        $invoice_detail = invoiceDetail::create($invoice_detail_data);
        }

        /*Customer Ledger Portion*/
        
        $customer_ledger = array("customer_id"=>$request["invoice_data"][0]["customer_id"],
            "invoice_id"=>$invoice_id, "invoice_type"=> "Sale Invoice",
            "desciption"=>"None",
            "debit"=>$request["invoice_data"][0]["total"],
            "credit"=>$request["invoice_data"][0]["paid"],
            "balance"=>$request["invoice_data"][0]["paid"] - $request["invoice_data"][0]["total"]);
        $save_customer_ledger = CustomerLedger::create($customer_ledger);
        
        /*Customer Ledger Portion End*/    

        //dd($invoice_detail);
    return json_encode($invoice_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(invoice $invoice)
    {
        //
        $id = $invoice["id"];
        $Obj=invoice::find($id);        //model invoice
        return response($Obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        DB::table('invoices') /*db table*/
        ->where('id',$request["id"])
        ->update(['name' => $request["name"], 
                'category' => $request["category"],
                'sale_price' => $request["sale_price"],
                'purchase_price' => $request["purchase_price"],
                'quantity' => $request["quantity"],
                'sold' => $request["sold"],
                'stock' => $request["stock"],                
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(invoice $invoice)
    {
        //
        $data = array();
        $data["title"] = "Blocked Invoices";
        return view('invoice.blocked', compact('data'));
    }

    public function InvoiceView($invoice_id)
    {
        $index = 0;
        $data = array();
        $data["title"] = "Invoice";
        $data["invoice"] = invoice::find($invoice_id);
        $data["invoice_detail"] = invoiceDetail::where("invoice_id",$invoice_id)->get();
        $data["customer"] = customer::find($data["invoice"]["customer_id"]);
        foreach ($data["invoice_detail"] as $invoice) {
            $product = product::find($invoice["product_id"]);
            $data["invoice_detail"][$index]["product_name"] = $product["name"];
            $index++;
        }
        //dd($data);
        return view('invoice.view',compact('data')); // invoice is a folder name
   
    }
}
