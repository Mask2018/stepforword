<?php

namespace App\Http\Controllers;

use App\customer;
use App\Device;
use App\Package;
use App\CustomerDevice;
use App\CustomerExpense;
use App\CustomerLedger;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile; 
use Storage;
use DB;
 
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["title"] = "Customer";
        $data["devices"] = Device::all();        
        $data["packages"] = Package::all();
        $data["customers"] = customer::all();
        return view('customer.index', compact('data'));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {           
        $customer = $request->all();
        $new_router=$request->input('serial_number');
        $new_router_string = implode(",",$new_router);
        $customer['serial_number'] = $new_router_string;

         /*return json_encode($customer['mac']);*/
        Storage::disk('local')->put($request->file, 'Contents');
      
        $customer_id = customer::create($customer)->id;

        
        /*Customer Ledger Portion*/
        
        $customer_ledger = array("customer_id"=>$customer_id, "invoice_id"=>$customer_id, "invoice_type"=> "OB", "desciption"=>"Opening Balance", "debit"=>0, "credit"=>0, "balance"=>$request->balance);
        //dd($customer_ledger);

        $save_customer_ledger = CustomerLedger::create($customer_ledger);
        
        /*Customer Ledger Portion End*/
        return redirect('/customer')->with('flash_success', 'Customer Saved Successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(customer $customer)
    {
        //
    }

   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $customer)
    { 
        $id = $customer["id"];
        $custObj=customer::find($id); 
        return response($custObj);

        //dd($custObj);
        /*return $id;*/
        //return view('customer.edit')->with('custObj',$custObj);
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

         DB::table('customers') /*db table*/
        ->where('id',$request["id"])
        ->update(['name' => $request["name"], 
                'cnic' => $request["cnic"],
                'balance' => $request["balance"],
                'address' => $request["address"],
                'mac' => $request["mac"],
                'ip' => $request["ip"],
                'area_location'=>"lhr",
                'serial_number'=>["serial_number"],
                'package' => $request["package"],
                'desciption' => $request["desciption"],
                'file' => $request["file"],
                'file_second' => $request["file_second"], 
            ]);
       /* return redirect('/customer')->with('flash_success', 'Customer Saved Successfully');
   */ }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(customer $customer)
    {
        //
    }

    public function blocked()
    {
        $data = array();
        $data["title"] = "Blocked Customers";
        return view('customer.blocked', compact('data'));
    }

    public function customer_ledger($id)
    { 
        $data = array();
        $data["title"] = "";
        $customer_name = customer::find($id);
        $data["customer_ledgers"] = CustomerLedger::where("customer_id",$id)->get();
       // dd($id);
        //dd($data["customer_ledgers"]);
        return view('customer.customerledger',compact('data','customer_name'));
    }
 
    public function customer_device($id)
    {
        $data = array();
        $data["title"] = "";
        $data["devices"] = Device::all();
        $data["packages"] = Package::all();
        $customer_name = customer::find($id);
        $data["customer_devices"] = CustomerDevice::where("customer_id",$id)->get();
        $data["customer_id"] = $id;
         // dd($data["customer_devices"]);
             /*db and model*/
         return view('customer.customerdevice',compact('data','customer_name'));
    }

    public function customer_device_store(Request $request)
    { 
       $customer_device = $request->all();
       $id = $customer_device['customer_id'];
       $customer_device = CustomerDevice::create($customer_device);
       return redirect('/customer_device/'.$id)->with('flash_success', 'Device Saved Successfully');
    }

    public function customer_device_edit(Request $CustomerDevice)
    { 
        $id = $CustomerDevice["id"];
        $custObj=CustomerDevice::find($id); 
        return response($custObj);
       // dd($custObj);
    }


     public function customer_device_update(Request $request)
    {

         DB::table('customer_expenses') /*db table*/
        ->where('id',$request["id"])
        ->update(['customer_name' => $request["customer_name"], 
                'name'          => $request["name"],
                'price'         => $request["price"],
                'description'   => $request["description"]
            ]);
       /* return redirect('/customer')->with('flash_success', 'Customer Saved Successfully');
   */
   } 

    public function customer_expense($id)
    { 
        $data = array();
        $data["title"] = "";
        $customer_name = customer::find($id);
        $date["customers"]=customer::all();
$data["customer_expenses"] =CustomerExpense::where("customer_id",$id)->get();
       $data["customer_id"] = $id;
        return view('customer.customerexpense',compact('data','customer_name'));
    }

    public function customer_expense_store(Request $request)
    { 
       $customer_expense = $request->all();
       $id = $customer_expense['customer_id'];
       $customer_expense = CustomerExpense::create($customer_expense);
       return redirect('/customer_expense/'.$id)->with('flash_success', 'Device Saved Successfully');
    }

    public function customer_expense_edit(Request $CustomerExpense)
    { 
        $id = $CustomerExpense["id"];
        $custObj=CustomerExpense::find($id); 
        return response($custObj);
       // dd($custObj);
    }


     public function customer_expense_update(Request $request)
    {

         DB::table('customer_expenses') /*db table*/
        ->where('id',$request["id"])
        ->update(['name'     => $request["name"], 
                'price'      => $request["price"],
                'description'=> $request["description"], 
            ]);
    }
} 