<?php

namespace App\Http\Controllers;

use App\vendor;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\VendorLedger;
use Storage;
use DB;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["title"] = "Vendor";
        $data["vendors"] = vendor::all();
        return view('vendor.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        $vendor = $request->all();
        Storage::disk('local')->put($request->file, 'Contents');
       // dd($vendor);
        //variable = modelname::funation($array);
        $vendor_id = vendor::create($vendor)->id;
        /*Vendor Ledger Portion*/
        
        $vendor_ledger = array("vendor_id"=>$vendor_id, "invoice_id"=>$vendor_id, "invoice_type"=> "OB", "desciption"=>"Opening Balance", "debit"=>0, "credit"=>0, "balance"=>$request->balance);
        //dd($vendor_ledger);
        $save_vendor_ledger = VendorLedger::create($vendor_ledger);
        
        /*Vendor Ledger Portion End*/
        return redirect('/vendor')->with('flash_success', 'Vendor Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(vendor $vendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $vendor)
    {
    
        $id = $vendor["id"];
        $venObj=vendor::find($id);
        
        return response($venObj);

        //dd($custObj);
        /*return $id;*/
        //return view('vendor.edit')->with('custObj',$custObj);
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

         DB::table('vendors') /*db table*/
        ->where('id',$request["id"])
        ->update(['name'       => $request["name"], 
                'cnic'         => $request["cnic"],
                'sale'         => $request["sale"],
                'purchase'     => $request["purchase"],
                'credit'       => $request["credit"],
                'balance'      => $request["balance"],
                'address'      => $request["address"],
                'package'      => $request["package"],
                'expenses'     => $request["expenses"],
                'area_location' => $request["area_location"],
                'desciption'   => $request["desciption"],   
            ]);
      //  return redirect('/vendor')->with('flash_success', 'Vendor Saved Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(vendor $vendor)
    {
        //
    }

    public function blocked()
    {
        $data = array();
        $data["title"] = "Blocked Vendors";
        return view('vendor.blocked', compact('data'));
    }

    public function vendor_ledger($id)
    { 
        $data = array();
        $data["title"] = "Vendor Ledger";
        $data["vendor_ledgers"] = VendorLedger::where("vendor_id",$id)->get();
       // dd($id);
        //dd($data["customer_ledgers"]);
        return view('vendor.vendorledger',compact('data'));
    }
}
