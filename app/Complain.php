<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $fillable = [
       'customer_id', 'title', 'desciption', 'assign_to', 'status','remarks'
    ];

    public function customer()
    {
    	return $this->belongsTo('App\customer','customer_id');
    }
}
