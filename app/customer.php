<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       	'name', 'cnic', 'number', 'due_date', 'balance', 'address', 'serial_number', 'package','desciption','file','file_second'
    ];
}
