<?php
   
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
 
Auth::routes();

Route::get('/home',				'HomeController@index')		->name('home'); 

Route::get('/customer',			'CustomerController@index')	->name('customer');
Route::get('/customer/blocked', 'CustomerController@blocked')->name('blocked_customer');
Route::post('/customer/store', 	'CustomerController@store')	->name('customer_store');
Route::post('/customer/edit', 	'CustomerController@edit')	->name('customer_edit');
Route::post('/customer/update', 'CustomerController@update')->name('customer_update');

Route::get('/customer/customerledger/{id}', 'CustomerController@customer_ledger')->name('customer_ledger');

Route::get('/customer_device/{id}', 'CustomerController@customer_device')->name('customer_device');
Route::post('/customer_device_store', 'CustomerController@customer_device_store')->name('customer_device_store');
Route::post('/customer_device_edit/', 'CustomerController@customer_device_edit')->name('customer_device_edit');
Route::post('/customer_device_update/', 'CustomerController@customer_device_update')->name('customer_device_update');


Route::get('/customer_expense/{id}',
	'CustomerController@customer_expense')->name('customer_expense');
Route::post('/customer_expense_store',
	'CustomerController@customer_expense_store')->name('customer_expense_store');
Route::post('/customer_expense_edit/',
	'CustomerController@customer_expense_edit')->name('customer_expense_edit');
Route::post('/customer_expense_update/','CustomerController@customer_expense_update')->name('customer_expense_update');


/*customer notification*/
Route::get('/clientrequest','ClientController@index')->name('clientrequest');
Route::post('/clientrequest/store', 	'ClientController@store')	->name('clientrequest_store');
Route::post('/clientrequest/edit', 	'ClientController@edit')	->name('clientrequest_edit');
Route::post('/clientrequest/update', 'ClientController@update')->name('clientrequest_update');



/*close*/

Route::get('/vendor', 			'VendorController@index')	->name('vendor');
Route::get('/vendor/blocked', 	'VendorController@blocked')	->name('blocked_vendor');
Route::post('/vendor/store',  	'VendorController@store')	->name('vendor_store');
Route::post('/vendor/edit',   	'VendorController@edit')	->name('vendor_edit');
Route::post('/vendor/update', 	'VendorController@update')	->name('vendor_update');
Route::get('/vendor/vendorledger/{id}','VendorController@vendor_ledger')	->name('vendor_ledger');

#shareholder routs
Route::get('/shareholder','ShareHolderController@index')->name('shareholder');
Route::get('/shareholder/blocked', 'ShareHolderController@blocked')	->name('blocked_shareholder');
Route::post('/shareholder/store', 'ShareHolderController@store')->name('shareholder_store');
Route::post('/shareholder/edit', 'ShareHolderController@edit')->name('shareholder_edit');
Route::post('/shareholder/update', 'ShareHolderController@update')->name('shareholder_update');
Route::get('/shareholder/shareholderledger/{id}','ShareHolderController@shareholder_ledger')->name('shareholder_ledger');



Route::get('/product', 			'ProductController@index')	->name('product');
Route::get('/product/blocked', 	'ProductController@blocked')->name('blocked_product');
Route::post('/product/store',  	'ProductController@store')	->name('product_store');
Route::post('/product/edit',   	'ProductController@edit')	->name('product_edit');
Route::post('/product/update', 	'ProductController@update')	->name('product_update');


Route::get('/invoice', 			'InvoiceController@index')	->name('invoice');
Route::get('/invoice/blocked', 	'InvoiceController@blocked')->name('blocked_invoice');
Route::post('/invoice/store',  	'InvoiceController@store')	->name('invoice_store');
Route::post('/invoice/edit',   	'InvoiceController@edit')	->name('invoice_edit');
Route::post('/invoice/update', 	'InvoiceController@update')	->name('invoice_update');
Route::get('/invoice/view/{id}','InvoiceController@InvoiceView')->name('invoice_view');
//Route::get('/invoice/view/',	'InvoiceController@InvoiceView')->name('invoice_view');

Route::get('/dp', 			'DpmanagmentController@index')	->name('dp');
Route::get('/dp/blocked', 	'DpmanagmentController@blocked')->name('blocked_dp');
Route::post('/dp/store',  	'DpmanagmentController@store')	->name('dp_store');
Route::post('/dp/edit',   	'DpmanagmentController@edit')	->name('dp_edit');
Route::post('/dp/update', 	'DpmanagmentController@update')	->name('dp_update');


Route::get('/device', 		  'DeviceController@index')	 ->name('device');
Route::get('/device/blocked', 'DeviceController@blocked')->name('blocked_device');
Route::post('/device/store',  'DeviceController@store')	 ->name('device_store');
Route::post('/device/edit',   'DeviceController@edit')	 ->name('device_edit');
Route::post('/device/update', 'DeviceController@update') ->name('device_update');



Route::get('/purchase', 'PurchaseController@index') ->name('purchase');
Route::get('/purchase/blocked', 'PurchaseController@blocked')->name('blocked_purchase');
Route::post('/purchase/store','PurchaseController@store')->name('purchase_store');
Route::post('/purchase/edit','PurchaseController@edit')->name('purchasee_edit');
Route::post('/purchase/update','PurchaseController@update')->name('purchase_update');
Route::get('/purchase/view/{id}','PurchaseController@InvoiceView')->name('purchase_view');


Route::get('/complain','ComplainController@index')->name('complain');
Route::get('/complain/blocked','ComplainController@blocked')->name('blocked_complain');
Route::post('/complain/store','ComplainController@store')->name('complain_store');
Route::post('/complain/edit','ComplainController@edit')	->name('complain_edit');
Route::post('/complain/update','ComplainController@update')->name('complain_update');

/* Expense */

Route::get('/expense', 'ExpenseController@index')->name('expense');
Route::get('/expense/blocked','ExpenseController@blocked')->name('blocked_expense');
Route::post('/expense/store', 'ExpenseController@store')->name('expense_store');
Route::post('/expense/edit',  'ExpenseController@edit')->name('expense_edit');
Route::post('/expense/update','ExpenseController@update')->name('expense_update');


/*  Payment Receipt*/

Route::get('/paid_receipt','PaidReceiptController@index')->name('paid_receipt');
Route::get('/paid_receipt/blocked','PaidReceiptController@blocked')->name('blocked_paid_receipt');
Route::post('/paid_receipt/store','PaidReceiptController@store')->name('paid_receipt_store');
Route::post('/paid_receipt/edit','PaidReceiptController@edit')->name('paid_receipt_edit');
Route::post('/paid_receipt/update','PaidReceiptController@update')->name('paid_receipt_update');


/* Received Receipt */

Route::get('/received_receipt','ReceivedReceiptController@index')->name('received_receipt');
Route::get('/received_receipt/blocked','ReceivedReceiptController@blocked')->name('blocked_received_receipt');
Route::post('/received_receipt/store','ReceivedReceiptController@store')->name('received_receipt_store');
Route::post('/received_receipt/edit_r','ReceivedReceiptController@edit')->name('received_receipt_edit');
Route::post('/received_receipt/update','ReceivedReceiptController@update')->name('received_receipt_update');



Route::get('/package', 			'PackageController@index')	->name(
		'package');
Route::get('/package/blocked', 	'PackageController@blocked')->name('blocked_package');
Route::post('/package/store',  	'PackageController@store')	->name('package_store');
Route::post('/package/edit',   	'PackageController@edit')	->name('package_edit');
Route::post('/package/update', 	'PackageController@update')	->name('package_update');